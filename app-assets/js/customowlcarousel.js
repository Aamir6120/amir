var timeout = '{{ $all_transition[0]->time_out }}';
// var speed = '{{ $all_transition[0]->speed }}';
$(document).ready(function(){
    // alert(timeout);
    $('.home-slider').owlCarousel({
        loop:true,
        autoplay:true,
        autoplayTimeout:5000,
        smartSpeed: 5000,
        margin:0,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        nav:false,
        dots: false,
        autoplayHoverPause: false,
        items: 1,
        navText : ["<span class='ion-md-arrow-back'></span>","<span class='ion-chevron-right'></span>"],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });
});