<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('name')->nullable();
            $table->string('business_name')->nullable();
            $table->string('broker_name')->nullable();
            $table->string('address')->nullable();
            $table->string('broker_address')->nullable();
            $table->boolean('licensed')->default(0);
            $table->string('licensed_info')->nullable();
            $table->string('specialty_type')->nullable();
            $table->integer('experience')->nullable();
            $table->string('website')->nullable();
            $table->boolean('insurance')->default(0);
            $table->string('insurance_type')->nullable();
            $table->boolean('employee_before')->default(0);
            $table->boolean('pay_workmans_comp_insurance')->default(0);
            $table->string('insurance_proof')->nullable();
            $table->boolean('work_with_investors_before')->default(0);
            $table->string('where_you_find_us')->nullable();
            $table->string('referral1_name')->nullable();
            $table->string('referral1_email')->nullable();
            $table->string('referral1_phone')->nullable();
            $table->string('referral2_name')->nullable();
            $table->string('referral2_email')->nullable();
            $table->string('referral2_phone')->nullable();
            $table->string('referral3_name')->nullable();
            $table->string('referral3_email')->nullable();
            $table->string('referral3_phone')->nullable();
            $table->string('is_individual')->nullable();
            $table->boolean('meet_accredited_investor_requirement')->nullable();
            $table->string('investment')->nullable();
            $table->string('started_date')->nullable();
            $table->string('money_borrow')->nullable();
            $table->string('project_type')->nullable();
            $table->string('loan_type')->nullable();
            $table->string('project_or_loan_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_data');
    }
}
