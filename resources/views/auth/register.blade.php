@extends('layouts.app')
@section('content')
    <div class="hero-wrap" style="background-image:url({{ url('app-assets/images/web-search.jpg') }});">
        {{--<div class="hero-wrap" style="background-color: #109fd1;">--}}
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    {{--                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Contact</span></p>--}}
                    <h1 class="mb-3 bread">Register</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section contact-section bg-light">
        <div class="container">
            <center>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="h2 font_style">Get Started</h2>
                        <p>Already registered? <a href="{{ url('login')  }}">Log In</a></p>
                    </div>
                </div>
            </center>
                 
                <div class="row block-9">
                    <div class="col-md-12">
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul style="list-style-type: none;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <div class="form-group">
                            <label class="h6 font-weight-bold">Please Select <span>*</span></label>
                            <select class="form-control" id="select_role" >
                                <option>Select</option>
                                <option value="2">Contractor</option>
                                <option value="3">Realtor</option>
                                <option value="4">Wholesaler</option>
                                <option value="5">Accredited Investor</option>
                                <option value="6">Investor</option>
                                <option value="7">Funding</option>
                            </select>
                        </div>
                    </div>
                    <div id="constructor" hidden>
                        <div class="container">
                            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Business Name <span>*</span></label>
                                            <input type="text" class="form-control" name="business_name" required placeholder="Business Name"/>
                                             @if ($errors->has('business_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('business_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Email <span>*</span></label>
                                            <input type="email" name="email" class="form-control" required placeholder="Email">
                                             @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <input type="hidden" name="role" value="2">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Password <span>*</span></label>
                                            <input type="password" name="password" required class="form-control" placeholder="password">
                                             @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Confirm Password <span>*</span></label>
                                            <input type="password" name="password_confirmation" class="form-control" placeholder="Re-type password">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                            <input type="text" name="phone" required class="form-control" placeholder="Phone No">
                                             @if ($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Address <span>*</span></label>
                                            <textarea class="form-control" rows="3" id="comment" name="address"></textarea>
                                             @if ($errors->has('address'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Specialty Type <span>*</span></label>
                                            <select class="form-control" name="specialty_type">
                                                <option>Select</option>
                                                <option value="general">General</option>
                                                <option value="roofer">Roofer</option>
                                                <option value="licensed_electrician">Licensed Electrician</option>
                                                <option value="licensed_plumber">Licensed Plumber</option>
                                                <option value="commercial">Commercial</option>
                                                <option value="remodel">Remodel</option>
                                                <option value="fix_flip">Fix & Flip</option>
                                                <option value="apartments">Apartments</option>
                                                <option value="handyman">Handyman</option>
                                                <option value="paint">Paint</option>
                                                <option value="demo_hauling">Demo and Hauling</option>
                                                <option value="new_build">New Build</option>
                                                <option value="apartment_build">Apartment Build</option>
                                                <option value="storage_unit">Storage Unit</option>
                                            </select>
                                             @if ($errors->has('specialty_type'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('specialty_type') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">How long have you been in business? <span>*</span></label>
                                            <input type="number" class="form-control" required placeholder="" name="experience">
                                             @if ($errors->has('experience'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('experience') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Website Url? <span>*</span></label>
                                            <input type="text" class="form-control" required name="website" placeholder="www.example.com">
                                             @if ($errors->has('website'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('website') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Licensed? <span>* &nbsp;</span></label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="yes" name="licensed" required class="custom-control-input" value="1">
                                            <label class="custom-control-label" for="yes">yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="no" name="licensed" required class="custom-control-input" value="0">
                                            <label class="custom-control-label" for="no">No</label>
                                             @if ($errors->has('licensed'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('licensed') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Licensed Info <span>*</span></label>
                                            <input type="text" class="form-control" required name="licensed_info" placeholder="Licensed Info">
                                             @if ($errors->has('licensed_info'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('licensed_info') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Are you insured? <span>*</span></label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="yes_insured" required name="insurance" class="custom-control-input" value="1">
                                            <label class="custom-control-label" for="yes_insured">yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="no_insured" required name="insurance" class="custom-control-input" value="0">
                                            <label class="custom-control-label" for="no_insured">No</label>
                                             @if ($errors->has('insurance'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('insurance') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Type of insurance <span>*</span></label>
                                            <input type="text" class="form-control" required placeholder="type of insurance" name="insurance_type">
                                             @if ($errors->has('insurance_type'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('insurance_type') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Do you have employees? <span>*</span></label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="yes_employees" required name="employee_before" class="custom-control-input" value="1">
                                            <label class="custom-control-label" for="yes_employees">yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="no_employees" required name="employee_before" class="custom-control-input" value="0">
                                            <label class="custom-control-label" for="no_employees">No</label>
                                             @if ($errors->has('employee_before'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('employee_before') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">How did you hear about our trusted partners program? <span>*</span></label>
                                            <select class="form-control" name="where_you_find_us">
                                                <option>Select</option>
                                                <option value="google">Google</option>
                                                <option value="facebook">Facebook</option>
                                                <option value="brian">Brian</option>
                                                <option value="jordon">Jordon</option>
                                                <option value="network event">Network Event</option>
                                            </select>
                                             @if ($errors->has('where_you_find_us'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('where_you_find_us') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Do you pay workman’s comp insurance? <span>*</span></label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="yes_pay" required name="pay_workmans_comp_insurance" class="custom-control-input" value="1">
                                            <label class="custom-control-label" for="yes_pay">yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="no_pay" required name="pay_workmans_comp_insurance" class="custom-control-input" value="0">
                                            <label class="custom-control-label" for="no_pay">No</label>
                                             @if ($errors->has('pay_workmans_comp_insurance'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('pay_workmans_comp_insurance') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Proof of insurance <span>*</span></label>
                                            <input type="file" class="form-control" required placeholder="Proof of insurance" name="insurance_proof">
                                             @if ($errors->has('insurance_proof'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('insurance_proof') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Have you worked with investors before? <span>*</span></label>

                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="yes_worked" required name="work_with_investors_before" class="custom-control-input" value="1">
                                            <label class="custom-control-label" for="yes_worked">yes</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="no_worked" required name="work_with_investors_before" class="custom-control-input" value="0">
                                            <label class="custom-control-label" for="no_worked">No</label>
                                             @if ($errors->has('work_with_investors_before'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('work_with_investors_before') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-2 mt-2">
                                        <h2 class="h4">Referral 1</h2>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Name <span>*</span></label>
                                            <input type="text" name="referral1_name" required class="form-control" placeholder="referral1_name">
                                             @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('referral1_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Email <span>*</span></label>
                                            <input type="text" name="referral1_email" required class="form-control" placeholder="email">
                                             @if ($errors->has('referral1_email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('referral1_email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                            <input type="text" name="referral1_phone" required class="form-control" placeholder="Phone No">
                                             @if ($errors->has('referral1_phone'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('referral1_phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-4">
                                        <h2 class="h4">Referral 2</h2>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Name <span>*</span></label>
                                            <input type="text" name="referral2_name" required class="form-control" placeholder="name">
                                             @if ($errors->has('referral2_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('referral2_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Email <span>*</span></label>
                                            <input type="text" name="referral2_email" required class="form-control" placeholder="email">
                                             @if ($errors->has('referral2_email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('referral2_email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                            <input type="text" name="referral2_phone" required class="form-control" placeholder="Phone No">
                                             @if ($errors->has('referral2_phone'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('referral2_phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12 mb-4">
                                        <h2 class="h4">Referral 3</h2>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Name <span>*</span></label>
                                            <input type="text" name="referral3_name" required class="form-control" placeholder="name">
                                             @if ($errors->has('referral3_name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('referral3_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Email <span>*</span></label>
                                            <input type="text" name="referral3_email" required class="form-control" placeholder="email">
                                             @if ($errors->has('referral3_email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('referral3_email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                            <input type="text" name="referral3_phone" required class="form-control" placeholder="Phone No">
                                             @if ($errors->has('referral3_phone'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('referral3_phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <h6>There is no obligation. We don't ever sell or share your information.</h6>
                                    </div>
                                    <div class="form-group container">
                                        <input type="submit" value="Submit" class="btn btn-primary py-3 px-5">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div id="realtor" hidden>
                        <div class="container">
                            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Your Name <span>*</span></label>
                                        <input type="text" name="name" required class="form-control" placeholder="Your Name"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Your Email <span>*</span></label>
                                        <input type="email" name="email" required class="form-control" placeholder="Your Email">
                                    </div>
                                </div>
                                <input type="hidden" name="role" value="3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Password <span>*</span></label>
                                        <input type="password" name="password" required class="form-control" placeholder="password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Confirm Password <span>*</span></label>
                                        <input type="password" name="password_confirmation" class="form-control" placeholder="Re-type password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Your Phone No <span>*</span></label>
                                        <input type="text" name="phone" class="form-control" required placeholder="Your Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Broker Name <span>*</span></label>
                                        <input type="text" name="broker_name" required class="form-control" placeholder="Broker Name"/>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Broker Address <span>*</span></label>
                                        <textarea class="form-control" required rows="3" id="comment" name="broker_address"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Specialty Type <span>*</span></label>
                                        <select class="form-control" name="specialty_type" required>
                                            <option>Select</option>
                                            <option value="buy">Buy</option>
                                            <option value="sell">Sell</option>
                                            <option value="rental">Rental</option>
                                            <option value="apartment">Apartment</option>
                                            <option value="commercial">Commercial</option>
                                            <option value="new_build">New Build</option>
                                            <option value="pre_sale">Pre-sale</option>
                                            <option value="comps">Comps</option>
                                            <option value="fix_flip">Fix & Flip</option>
                                            <option value="apartments">Apartments</option>
                                            <option value="storage_units">Storage Units</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Website Url? <span>*</span></label>
                                        <input type="text" class="form-control" required name="website" placeholder="www.example.com">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Licensed? <span>* &nbsp;</span></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="yes_licenses" required name="licensed" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="yes_licenses">yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="no_licenses" required name="licensed" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="no_licenses">No</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Licensed Info <span>*</span></label>
                                        <input type="text" class="form-control" required name="licensed_info" placeholder="Licensed Info">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">How long have you been in a real estate agent? <span>*</span></label>
                                        <input type="number" class="form-control" required placeholder="Licensed Info" name="experience">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">How did you hear about our trusted partners program? <span>*</span></label>
                                        <select class="form-control" name="where_you_find_us" required>
                                            <option>Select</option>
                                            <option value="google">Google</option>
                                            <option value="facebook">Facebook</option>
                                            <option value="brian">Brian</option>
                                            <option value="jordon">Jordon</option>
                                            <option value="network event">Network Event</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Have you worked with investors before? <span>*</span></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="yes_works" required name="work_with_investors_before" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="yes_works">yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="no_works" required name="work_with_investors_before" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="no_works">No</label>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-2 mt-2">
                                    <h2 class="h4">Referral 1</h2>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Name <span>*</span></label>
                                        <input type="text" name="referral1_name" required class="form-control" placeholder="name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Email <span>*</span></label>
                                        <input type="text" name="referral1_email" required class="form-control" placeholder="email">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                        <input type="text" name="referral1_phone" required class="form-control" placeholder="Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <h2 class="h4">Referral 2</h2>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Name <span>*</span></label>
                                        <input type="text" name="referral2_name" required class="form-control" placeholder="name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Email <span>*</span></label>
                                        <input type="text" name="referral2_email" required class="form-control" placeholder="email">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                        <input type="text" name="referral2_phone" required class="form-control" placeholder="Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <h2 class="h4">Referral 3</h2>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Name <span>*</span></label>
                                        <input type="text" name="referral3_name" required class="form-control" placeholder="name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Email <span>*</span></label>
                                        <input type="text" name="referral3_email" required class="form-control" placeholder="email">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                        <input type="text" name="referral3_phone" required class="form-control" placeholder="Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h6>There is no obligation. We don't ever sell or share your information.</h6>
                                </div>
                                <div class="form-group container">
                                    <input type="submit" value="Submit" class="btn btn-primary py-3 px-5">
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div id="wholesaler" hidden>
                        <div class="container">
                            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Business Name <span>*</span></label>
                                        <input type="text" name="business_name" required class="form-control" placeholder="Business  Name"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Email <span>*</span></label>
                                        <input type="email" name="email" required class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <input type="hidden" name="role" value="4">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Password <span>*</span></label>
                                        <input type="password" name="password" required class="form-control" placeholder="password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Confirm Password <span>*</span></label>
                                        <input type="password" name="password_confirmation" required class="form-control" placeholder="Re-type password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                        <input type="text" name="phone" required class="form-control" placeholder="Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Address <span>*</span></label>
                                        <textarea class="form-control" required name="address" rows="3" id="comment" name="address"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Specialty Type <span>*</span></label>
                                        <select class="form-control" name="specialty_type" required>
                                            <option>Select</option>
                                            <option value="getting_started">Getting Started</option>
                                            <option value="cash_buyer">Cash Buyer</option>
                                            <option value="distressed_property">Distressed Property</option>
                                            <option value="divorced_property">Divorced Property</option>
                                            <option value="paper_flipper">Paper Flipper</option>
                                            <option value="property_scout">Property Scout</option>
                                            <option value="referral_partner">Referral Partner</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Website Url? <span>*</span></label>
                                        <input type="text" class="form-control" required name="website" placeholder="www.example.com">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Licensed? <span>* &nbsp;</span></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="yes_wholesaler" required name="licensed" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="yes_wholesaler">yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="no_wholesaler" required name="licensed" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="no_wholesaler">No</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Licensed Info <span>*</span></label>
                                        <input type="text" class="form-control" required name="licensed_info" placeholder="Licensed Info">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">How long have you been in wholesaling? <span>*</span></label>
                                        <input type="number" class="form-control" required placeholder="Licensed Info" name="experience">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">How did you hear about our trusted partners program? <span>*</span></label>
                                        <select class="form-control" name="where_you_find_us" required>
                                            <option>Select</option>
                                            <option value="google">Google</option>
                                            <option value="facebook">Facebook</option>
                                            <option value="brian">Brian</option>
                                            <option value="jordon">Jordon</option>
                                            <option value="network event">Network Event</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Have you worked with investors before? <span>*</span></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="yes_saler" required name="work_with_investors_before" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="yes_saler">yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="no_saler" required name="work_with_investors_before" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="no_saler">No</label>
                                    </div>
                                </div>
                                <div class="col-md-12 mb-2 mt-2">
                                    <h2 class="h4">Referral 1</h2>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Name <span>*</span></label>
                                        <input type="text" name="referral1_name" required class="form-control" placeholder="name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Email <span>*</span></label>
                                        <input type="text" name="referral1_email" required class="form-control" placeholder="email">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                        <input type="text" name="referral1_phone" required class="form-control" placeholder="Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <h2 class="h4">Referral 2</h2>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Name <span>*</span></label>
                                        <input type="text" name="referral2_name" required class="form-control" placeholder="name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Email <span>*</span></label>
                                        <input type="text" name="referral2_email" required class="form-control" placeholder="email">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                        <input type="text" name="referral2_phone" required class="form-control" placeholder="Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12 mb-4">
                                    <h2 class="h4">Referral 3</h2>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Name <span>*</span></label>
                                        <input type="text" name="referral3_name" required class="form-control" placeholder="name">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Email <span>*</span></label>
                                        <input type="text" name="referral3_email" required class="form-control" placeholder="email">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                        <input type="text" name="referral3_phone" required class="form-control" placeholder="Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h6>There is no obligation. We don't ever sell or share your information.</h6>
                                </div>
                                <div class="form-group container">
                                    <input type="submit" value="Submit" class="btn btn-primary py-3 px-5">
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div id="accredited_investor" hidden>
                        <div class="container">
                            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Name <span>*</span></label>
                                        <input type="text" name="name" required class="form-control" placeholder="Name"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Email <span>*</span></label>
                                        <input type="email" name="email" required class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <input type="hidden" name="role" value="5">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Password <span>*</span></label>
                                        <input type="password" name="password" required class="form-control" placeholder="password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Confirm Password <span>*</span></label>
                                        <input type="password" name="password_confirmation" required class="form-control" placeholder="Re-type password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                        <input type="text" name="phone" required class="form-control" placeholder="Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Address <span>*</span></label>
                                        <textarea class="form-control" rows="3" id="comment" name="address"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Are you an individual or Corporation? <span>*</span></label>
                                        <input type="text" name="is_individual" required class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Do you meet the requirements to be an Accredited Investor? <span>* &nbsp;</span></label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="yes_requirements" required name="meet_accredited_investor_requirement" class="custom-control-input" value="1">
                                        <label class="custom-control-label" for="yes_requirements">yes</label>
                                    </div>
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="no_requirements" required name="meet_accredited_investor_requirement" class="custom-control-input" value="0">
                                        <label class="custom-control-label" for="no_requirements">No</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">How much money would you like to invest? <span>*</span></label>
                                        <input type="text" name="investment" required class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">When would you like to get started? <span>*</span></label>
                                        <input type="date" name="started_date" required class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">How did you hear about our trusted partners program? <span>*</span></label>
                                        <select class="form-control" name="where_you_find_us" required>
                                            <option>Select</option>
                                            <option value="google">Google</option>
                                            <option value="facebook">Facebook</option>
                                            <option value="brian">Brian</option>
                                            <option value="jordon">Jordon</option>
                                            <option value="network event">Network Event</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h6>There is no obligation. We don't ever sell or share your information.</h6>
                                </div>
                                <div class="form-group container">
                                    <input type="submit" value="Submit" class="btn btn-primary py-3 px-5">
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div id="investor" hidden>
                        <div class="container">
                            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Name <span>*</span></label>
                                        <input type="text" name="name" required class="form-control" placeholder="Name"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Email <span>*</span></label>
                                        <input type="email" name="email" required class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <input type="hidden" name="role" value="6">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Password <span>*</span></label>
                                        <input type="password" name="password" required class="form-control" placeholder="password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Confirm Password <span>*</span></label>
                                        <input type="password" name="password_confirmation" required class="form-control" placeholder="Re-type password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                        <input type="text" name="phone" class="form-control" required placeholder="Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Address <span>*</span></label>
                                        <textarea class="form-control" rows="3" id="comment" name="address"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Are you an individual or Corporation? <span>*</span></label>
                                        <input type="text" name="is_individual" required class="form-control" placeholder="">
                                    </div>
                                </div>
    {{--                            <div class="col-md-6">--}}
    {{--                                <div class="form-group">--}}
    {{--                                    <label class="h6 font-weight-bold">How much money would you like to invest? <span>*</span></label>--}}
    {{--                                    <input type="text" class="form-control" placeholder="">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                            <div class="col-md-6">--}}
    {{--                                <div class="form-group">--}}
    {{--                                    <label class="h6 font-weight-bold">When would you like to get started? <span>*</span></label>--}}
    {{--                                    <input type="date" class="form-control" placeholder="">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">How did you hear about our trusted partners program? <span>*</span></label>
                                        <select class="form-control" name="where_you_find_us" required>
                                            <option>Select</option>
                                            <option value="google">Google</option>
                                            <option value="facebook">Facebook</option>
                                            <option value="brian">Brian</option>
                                            <option value="jordon">Jordon</option>
                                            <option value="network event">Network Event</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h6>There is no obligation. We don't ever sell or share your information.</h6>
                                </div>
                                <div class="form-group container">
                                    <input type="submit" value="Submit" class="btn btn-primary py-3 px-5">
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <div id="funding" hidden>
                        <div class="container">
                            <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Name <span>*</span></label>
                                        <input type="text" name="name" required class="form-control" placeholder="Name"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Email <span>*</span></label>
                                        <input type="email" name="email" required class="form-control" placeholder="Email">
                                    </div>
                                </div>
                                <input type="hidden" name="role" value="7">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Password <span>*</span></label>
                                        <input type="password" name="password" required class="form-control" placeholder="password">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Confirm Password <span>*</span></label>
                                        <input type="password" name="password_confirmation" required class="form-control" placeholder="Re-type password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Phone No <span>*</span></label>
                                        <input type="text" name="phone" required class="form-control" placeholder="Phone No">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Address <span>*</span></label>
                                        <textarea class="form-control" rows="3" id="comment" name="address"></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">How much money would you like to borrow? <span>*</span></label>
                                        <input type="text" name="money_borrow" required class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Project type <span>*</span></label>
                                        <select class="form-control" name="project_type">
                                            <option>Select</option>
                                            <option value="land">Land</option>
                                            <option value="single_family">Single Family</option>
                                            <option value="multi_family">Multi-Family</option>
                                            <option value="commercial">Commercial</option>
                                            <option value="fix_flip">Fix and Flip</option>
                                            <option value="othe">other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">Loan Type <span>*</span></label>
                                        <select class="form-control" name="loan_type" required>
                                            <option>Select</option>
                                            <option value="purchase_loan">Purchase Loan</option>
                                            <option value="construction_loan">Construction Loan</option>
                                            <option value="Bridge_Gap_Loan">Bridge or Gap Loan</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="h6 font-weight-bold">What is the project or loan address? <span>*</span></label>
                                        <input type="text" name="project_or_loan_address" required class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h6>There is no obligation. We don't ever sell or share your information.</h6>
                                </div>
                                <div class="form-group container">
                                    <input type="submit" value="Submit" class="btn btn-primary py-3 px-5">
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
        </div>
    </section>
@endsection
@section('scripts')

        $(document).ready(function() {
            $("#select_role").change(function() {
                var selectedRole = $(this).children("option:selected").val();
                if(selectedRole == '2')
                {
                    $("#constructor").removeAttr("hidden");
                    $("#realtor").attr("hidden", "hidden");
                    $("#wholesaler").attr("hidden", "hidden");
                    $("#accredited_investor").attr("hidden", "hidden");
                    $("#investor").attr("hidden", "hidden");
                    $("#funding").attr("hidden", "hidden");
                }
                else if(selectedRole == '3')
                {
                    $("#constructor").attr("hidden", "hidden");
                    $("#realtor").removeAttr("hidden");
                    $("#wholesaler").attr("hidden", "hidden");
                    $("#accredited_investor").attr("hidden", "hidden");
                    $("#investor").attr("hidden", "hidden");
                    $("#funding").attr("hidden", "hidden");
                }
                else if(selectedRole == '4')
                {
                    $("#constructor").attr("hidden", "hidden");
                    $("#realtor").attr("hidden", "hidden");
                    $("#wholesaler").removeAttr("hidden");
                    $("#accredited_investor").attr("hidden", "hidden");
                    $("#investor").attr("hidden", "hidden");
                    $("#funding").attr("hidden", "hidden");
                }
                else if(selectedRole == '5')
                {
                    $("#constructor").attr("hidden", "hidden");
                    $("#realtor").attr("hidden", "hidden");
                    $("#wholesaler").attr("hidden", "hidden");
                    $("#accredited_investor").removeAttr("hidden");
                    $("#investor").attr("hidden", "hidden");
                    $("#funding").attr("hidden", "hidden");
                }
                else if(selectedRole == '6')
                {
                    $("#constructor").attr("hidden", "hidden");
                    $("#realtor").attr("hidden", "hidden");
                    $("#wholesaler").attr("hidden", "hidden");
                    $("#accredited_investor").attr("hidden", "hidden");
                    $("#investor").removeAttr("hidden");
                    $("#funding").attr("hidden", "hidden");
                }
                else if(selectedRole == '7')
                {
                    $("#constructor").attr("hidden", "hidden");
                    $("#realtor").attr("hidden", "hidden");
                    $("#wholesaler").attr("hidden", "hidden");
                    $("#accredited_investor").attr("hidden", "hidden");
                    $("#investor").attr("hidden", "hidden");
                    $("#funding").removeAttr("hidden");
                }
                else
                {
                    $("#constructor").attr("hidden", "hidden");
                    $("#realtor").attr("hidden", "hidden");
                    $("#wholesaler").attr("hidden", "hidden");
                    $("#accredited_investor").attr("hidden", "hidden");
                    $("#investor").attr("hidden", "hidden");
                    $("#funding").attr("hidden", "hidden");
                }
            });
        });

@endsection