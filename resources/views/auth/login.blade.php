@extends('layouts.app')
@section('content')
    <div class="hero-wrap" style="background-image:url({{ url('app-assets/images/web-search.jpg') }});">
        {{--<div class="hero-wrap" style="background-color: #109fd1;">--}}
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    {{--                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home</a></span> <span>Contact</span></p>--}}
                    <h1 class="mb-3 bread">Login</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section contact-section bg-light">
        <div class="container">
            <center>
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="h2 font_style">Login</h2>
                    </div>
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                   
                </div>

            </center>
            <form method="POST" action="{{ route('login') }}" class="bg-white p-5">
                @csrf
                        @if ($errors->any())
                            <div class="label label-danger text-center">
                                <ul style="list-style-type: none;">
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                <div class="row block-9">
                    <div class="col-md-12">
                        <div class="form-group">
                            <lable class="h6 font-weight-bold">Email Address <span>*</span></lable>
                            <input type="email" name="email" class="form-control" placeholder="example@gmail.com" required />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <lable class="h6 font-weight-bold">Password <span>*</span></lable>
                            <input type="password" name="password" class="form-control" placeholder="Password" required />
                        </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="custom-control custom-checkbox my-1 mr-sm-2">
                            <input type="checkbox" class="custom-control-input" id="customControlInline">
                            <label class="custom-control-label" for="customControlInline">Remember Me</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Login" class="btn btn-primary py-3 px-5">
                    </div>
{{--                    <p>Forgot Password | &nbsp;<a href="{{ url('/signup')  }}">Register</a></p>--}}
                </div>
            </form>
        </div>
    </section>
@endsection