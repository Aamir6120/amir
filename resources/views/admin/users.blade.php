@extends('admin.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
         <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="dashboard-ecommerce">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Users</h2>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
{{--                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Admin</a></li>--}}
                                        <li class="breadcrumb-item active" aria-current="page">Users</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">All Users</h5>
                            <div class="card-body">
{{--                                <div class="table-responsive">--}}
                                    <table id="load_datatable" class="table table-striped table-bordered first" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Sr.</th>
                                                <th>Phone</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>status</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
{{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#load_datatable').DataTable({
                "pageLength":25,
                "order": [[0, 'desc']],
                processing: true,
                serverSide: true,
                responsive: true,
                "initComplete": function (settings, json) {
                },

                ajax: "{!! Route('loadUsers') !!}",
                columns: [
                    {data: 'count', name: 'count'},
                    {data: 'phone', name: 'phone'},
                    {data: 'email', name: 'email'},
                    {data: 'Role', name: 'Role', orderable: false},
                    {data: 'status', name: 'status'},
                    {data: 'action', name: 'action', orderable: false},
                    // {data: 'action', name: 'action', orderable: false, searchable: false}
                    //{ data: 'updated_at', name: 'updated_at' }
                ]
            });
});
        function refreshTable() {
            $('#load_datatable').DataTable().ajax.reload();
        }
        

        function active_user(id){

           $.ajax({
                url: "{!! Route('active_user') !!}",
                type: 'GET',
                data: {id, id},
                dataType: 'json',

                success: function(data) {
                    refreshTable();

                }       
            })   
           
        }

        function pending_user(id){
            $.ajax({
                url: "{!! Route('pending_user') !!}",
                type: 'GET',
                data: {id, id},
                dataType: 'json',

                success: function(data) {
                    refreshTable();

                }       
            }) 
        }

        function delete_user(id){
            $.ajax({
                url: "{!! Route('delete_user') !!}",
                type: 'GET',
                data: {id, id},
                dataType: 'json',

                success: function(data) {
                    refreshTable();

                }       
            }) 
        }
    </script>

@endsection









