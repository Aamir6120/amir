@extends('admin.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
         <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="dashboard-ecommerce">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-8">
                        <div class="page-header">
                            <h2 class="pageheader-title">Listing</h2>
                            <p class="pageheader-text"></p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Listing</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-2">
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" style="float: right;">Add Transitions</button>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-2">
                        <a href="{{route('new_listing')}}"><button type="button" class="btn btn-primary" style="float: right;">Add New Listing</button></a>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Add Transitions</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form class="needs-validation" method="" action="" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="inputWebSite" class="col-3 col-lg-2 col-form-label text-right">Speed</label>
                                        <div class="col-6 col-lg-8">
                                            <input type="number" name="speed" id="speed"  class="form-control" value="{{$transition[0]->speed}}"><span></span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="time_out" class="col-3 col-lg-2 col-form-label text-right">TimeOut</label>
                                        <div class="col-6 col-lg-8">
                                            <input type="number" name="time_out" id="time_out" value="{{$transition[0]->time_out}}" class="form-control"><span></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="close_modal" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" onclick="transition_button()" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">All Listings</h5>
                        <div class="card-body">
                                <table id="load_datatable" class="table table-striped table-bordered first" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Sr.</th>
                                        <th>Title</th>
                                        <th>Location</th>
                                        <th>Detail</th>
                                        <th>price</th>
                                        <th>picture</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                        </div>
                    </div>
                </div>
@endsection
@section('scripts')
    <script>

        function transition_button(){
            var speed = $('#speed').val();
            var time_out = $('#time_out').val();
            $.ajax({
                url: "{!! Route('transition') !!}",
                type: 'POST',
                data: { 'speed'  : speed, 'time_out': time_out, '_token': '{{csrf_token()}}'},
                dataType: 'json',
                success: function(data) {
                    $('#close_modal').click();
                }       
            })

        }
    function refreshTable() {
            $('#load_datatable').DataTable().ajax.reload();
        }
    $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#load_datatable').DataTable({
                "pageLength":25,
                "order": [[0, 'asc']],
                processing: true,
                serverSide: true,
                responsive: true,
                "initComplete": function (settings, json) {
                },

                ajax: "{!! Route('all_listings') !!}",
                columns: [
                    {data: 'count', name: 'count'},
                    {data: 'title', name: 'title'},
                    {data: 'location', name: 'location'},
                    {data: 'description', name: 'description'},
                    {data: 'price', name: 'price'},
                    {data: 'image', name: 'image', orderable: false},
                     {data: 'action', name: 'action', orderable: false, searchable: false}
                    //{ data: 'updated_at', name: 'updated_at' }
                ]
            });
    });


    function delete_listing(id){
        $.ajax({
            url: "{!! Route('delete_listing') !!}",
            type: 'GET',
            data: {id, id},
            dataType: 'json',
            success: function(data) {
                refreshTable();

            }       
        }) 
    }

    function pause_listing(id){
        $.ajax({
            url: "{!! Route('pause_listing') !!}",
            type: 'GET',
            data: {id, id},
            dataType: 'json',
            success: function(data) {
                console.log(data);
                refreshTable();

            }       
        }) 
    }

    function play_listing(id){
        $.ajax({
            url: "{!! Route('play_listing') !!}",
            type: 'GET',
            data: {id, id},
            dataType: 'json',
            success: function(data) {
                console.log(data);
                refreshTable();

            }       
        }) 
    }

    </script>

@endsection









