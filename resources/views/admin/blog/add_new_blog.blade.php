@extends('admin.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="dashboard-ecommerce">
            <div class="container-fluid  dashboard-content">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Add New Post </h2>
                            <p class="pageheader-text"></p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{url('admin')}}" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item"><span class="breadcrumb-link">Add Post</span></li>                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
             
                    <div class="row">
                        <!-- ============================================================== -->
                        <!-- validation form -->
                        <!-- ============================================================== -->
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Post Detail</h5>
                                <div class="card-body">
                                    <form class="needs-validation" method="post" action="{{route('new_post')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="inputWebSite" class="col-3 col-lg-2 col-form-label text-right">Picture</label>
                                            <div class="col-9 col-lg-10">
                                                <input type="file" name="picture"  class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail2" class="col-3 col-lg-2 col-form-label text-right">Title</label>
                                            <div class="col-9 col-lg-10">
                                                <input type="text" name="title" class="form-control" id="title" placeholder="title" required="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-3 col-lg-2 col-form-label text-right">Detail</label>
                                            <div class="col-9 col-lg-10">
                                                <textarea name="detail" id="modern_textarea" class="form-control" placeholder="Detail" required="">
                                                    <p>Write Content Here ....</p>
                                                </textarea>
                                            </div>
                                        </div>
                                        <div class="row pt-2 pt-sm-5 mt-1">
                                            <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0"></div>
                                            <div class="col-sm-6 pl-0">
                                                <p class="text-right">
                                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
@endsection
@section('scripts')
    <script>

        $(document).ready(function() {

            if ($("#modern_textarea").length > 0) {
                tinymce.init({
                    selector: "textarea#modern_textarea",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

                });
            }
        });

    </script>

@endsection

