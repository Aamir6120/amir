@extends('admin.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
         <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
        <div class="dashboard-ecommerce">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <a href="{{route('add_review')}}"><button type="button" class="btn btn-primary" style="float: right;">Add New Review</button></a>
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Reviews</h2>
                            <p class="pageheader-text"></p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{url('admin')}}" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Reviews</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="card">
                        <h5 class="card-header">All Posts</h5>
                        <div class="card-body">
                                <table id="load_datatable" class="table table-striped table-bordered first" style="width: 100%;">
                                    <thead>
                                    <tr>
                                        <th>Sr.</th>
                                        <th>Title</th>
                                        <th>Role</th>
                                        <th>Detail</th>
                                        <th>picture</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                        </div>
                    </div>
                </div>
@endsection
@section('scripts')
    <script>

    function refreshTable() {
            $('#load_datatable').DataTable().ajax.reload();
        }
    $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#load_datatable').DataTable({
                "pageLength":25,
                "order": [[0, 'asc']],
                processing: true,
                serverSide: true,
                responsive: true,
                "initComplete": function (settings, json) {
                },

                ajax: "{!! Route('get_review') !!}",
                columns: [
                    {data: 'count', name: 'count'},
                    {data: 'title', name: 'title'},
                    {data: 'role', name: 'role'},
                    {data: 'description', name: 'description'},
                    {data: 'image', name: 'image', orderable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                    //{ data: 'updated_at', name: 'updated_at' }
                ]
            });
    });


    function delete_review_post(id){
        $.ajax({
            url: "{!! Route('delete_review_post') !!}",
            type: 'GET',
            data: {id, id},
            dataType: 'json',
            success: function(data) {
                refreshTable();

            }       
        }) 
    }

    </script>

@endsection









