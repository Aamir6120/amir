@extends('admin.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="dashboard-ecommerce">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">User</h2>
                            <p class="pageheader-text">Nulla euismod urna eros, sit amet scelerisque torton lectus vel mauris facilisis faucibus at enim quis massa lobortis rutrum.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Users</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="ecommerce-widget">

                    <div class="row">
                        
                        
                        
                        
                        
                        
                    </div>
                </div>
                    <div class="row">
                        
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="card">
                                    <div class="card-header">
                                        User Detail
                                    </div>
                                    <div class="card-body">
                                         <div class="table-responsive">
                                            <table class="table">
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{ $user->phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user->email }}</td>
                                                </tr>

                                            @if($user->role == 2)
                                                <tr>
                                                    <td>Business Name</td>
                                                    <td>{{ $user_data->business_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Address</td>
                                                    <td>{{ $user_data->address }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Licensed</td>
                                                    <td>@if( $user_data->licensed  == 1)  Yes @else  No  @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Licensed Info</td>
                                                    <td>{{ $user_data->licensed_info }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Specialty Type</td>
                                                    <td>{{ $user_data->specialty_type }}</td>
                                                </tr>
                                                <tr>
                                                    <td>How long have you been in this Business</td>
                                                    <td>{{ $user_data->experience }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Website</td>
                                                    <td>{{ $user_data->website }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Insurance</td>
                                                    <td>{{ $user_data->insurance }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Insurance Type</td>
                                                    <td>{{ $user_data->insurance_type }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Do you have employees</td>
                                                    <td>{{ $user_data->employee_before }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Do you pay workman’s comp insurance</td>
                                                    <td>{{ $user_data->pay_workmans_comp_insurance }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Proof of insurance</td>
                                                    <td>{{ $user_data->insurance_proof }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Have you worked with investors before</td>
                                                    <td>@if($user_data->work_with_investors_before ==1) Yes @else No @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Referral 1:</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->referral1_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user_data->referral1_email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{ $user_data->referral1_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Referral 2:</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->referral2_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user_data->referral2_email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{ $user_data->referral2_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Referral 3:</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->referral3_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user_data->referral3_email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{ $user_data->referral3_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>How did you hear about our trusted partners program</td>
                                                    <td>{{ $user_data->where_you_find_us }}</td>
                                                </tr>
                                            @endif
                                            @if($user->role == 3)

                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Broker Name</td>
                                                    <td>{{ $user_data->broker_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Broker Address</td>
                                                    <td>{{ $user_data->broker_address }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Licensed</td>
                                                    <td>@if( $user_data->licensed  == 1)  Yes @else No  @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Licensed Info</td>
                                                    <td>{{ $user_data->licensed_info }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Specialty Type</td>
                                                    <td>{{ $user_data->specialty_type }}</td>
                                                </tr>
                                                <tr>
                                                    <td>How long have you been in a real estate agent</td>
                                                    <td>{{ $user_data->experience }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Website</td>
                                                    <td>{{ $user_data->website }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Have you worked with investors before</td>
                                                    <td>@if($user_data->work_with_investors_before ==1) Yes @else No @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Referral 1:</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->referral1_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user_data->referral1_email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{ $user_data->referral1_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Referral 2:</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->referral2_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user_data->referral2_email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{ $user_data->referral2_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Referral 3:</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->referral3_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user_data->referral3_email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{ $user_data->referral3_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>How did you hear about our trusted partners program</td>
                                                    <td>{{ $user_data->where_you_find_us }}</td>
                                                </tr>
                                            @endif
                                            @if($user->role == 4)

                                                <tr>
                                                    <td>Business Name</td>
                                                    <td>{{ $user_data->business_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Address</td>
                                                    <td>{{ $user_data->address }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Licensed</td>
                                                    <td>@if( $user_data->licensed  == 1) Yes @else {No  @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Licensed Info</td>
                                                    <td>{{ $user_data->licensed_info }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Specialty Type</td>
                                                    <td>{{ $user_data->specialty_type }}</td>
                                                </tr>

                                                <tr>
                                                    <td>How long have you been in wholesaling.</td>
                                                    <td>{{ $user_data->experience }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Website</td>
                                                    <td>{{ $user_data->website }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Have you worked with investors before</td>
                                                    <td>@if($user_data->work_with_investors_before ==1) Yes @else No @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>Referral 1:</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->referral1_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user_data->referral1_email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{ $user_data->referral1_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Referral 2:</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->referral2_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user_data->referral2_email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{ $user_data->referral2_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Referral 3:</td>
                                                </tr>
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->referral3_name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Email</td>
                                                    <td>{{ $user_data->referral3_email }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Phone</td>
                                                    <td>{{ $user_data->referral3_phone }}</td>
                                                </tr>
                                                <tr>
                                                    <td>How did you hear about our trusted partners program</td>
                                                    <td>{{ $user_data->where_you_find_us }}</td>
                                                </tr>
                                                
                                            @endif

                                            @if($user->role == 5)
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Address</td>
                                                    <td>{{ $user_data->address }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Are you an individual or Corporation</td>
                                                    <td>{{ $user_data->is_individual }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Do you meet the requirements to be an Accredited Investor</td>
                                                    <td>@if($user_data->meet_accredited_investor_requirement == 1) Yes @else No @endif</td>
                                                </tr>
                                                <tr>
                                                    <td>How much money would you like to invest?</td>
                                                    <td>{{ $user_data->investment }}</td>
                                                </tr>
                                                <tr>
                                                    <td>When would you like to get started?</td>
                                                    <td>{{ $user_data->started_date }}</td>
                                                </tr>
                                                <tr>
                                                    <td>How did you hear about our trusted partners program</td>
                                                    <td>{{ $user_data->where_you_find_us }}</td>
                                                </tr>

                                            @endif
                                            @if($user->role == 6)
                                                 <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->name }}</td>
                                                </tr>
                                                 <tr>
                                                    <td>Address</td>
                                                    <td>{{ $user_data->address }}</td>
                                                </tr>
                                                 <tr>
                                                    <td>Are you an individual or Corporation</td>
                                                    <td>{{ $user_data->is_individual }}</td>
                                                </tr>
                                                 <tr>
                                                    <td>How did you hear about our trusted partners program</td>
                                                    <td>{{ $user_data->where_you_find_us }}</td>
                                                </tr>
                                            @endif

                                            @if($user->role == 7)
                                                <tr>
                                                    <td>Name</td>
                                                    <td>{{ $user_data->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Address</td>
                                                    <td>{{ $user_data->address }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Are you an individual or Corporation</td>
                                                    <td>{{ $user_data->is_individual }}</td>
                                                </tr>
                                                <tr>
                                                    <td>How much money would you like to borrow?</td>
                                                    <td>{{ $user_data->money_borrow }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Project type</td>
                                                    <td>{{ $user_data->project_type }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Loan typ</td>
                                                    <td>{{ $user_data->loan_type }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Project or loan address</td>
                                                    <td>{{ $user_data->project_or_loan_address }}</td>
                                                </tr>
                                            @endif
                                        </table>
                                    </div>
                                    </div>
                                </div>
                           
                            
                    
               

@endsection