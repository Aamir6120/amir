<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ url('app-assets/admin/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link href="{{ url('app-assets/admin/vendor/fonts/circular-std/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('app-assets/admin/libs/css/style.css') }}">
    <link rel="stylesheet" href="{{ url('app-assets/admin/vendor/fonts/fontawesome/css/fontawesome-all.css') }}">
    <link rel="stylesheet" href="{{ url('app-assets/admin/vendor/charts/chartist-bundle/chartist.css') }}">
    <link rel="stylesheet" href="{{ url('app-assets/admin/vendor/charts/morris-bundle/morris.css') }}">
    <link rel="stylesheet" href="{{ url('app-assets/admin/vendor/fonts/material-design-iconic-font/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">

    <link rel="stylesheet" href="{{ url('app-assets/admin/vendor/charts/c3charts/c3.css') }}">
    <link rel="stylesheet" href="{{ url('app-assets/admin/vendor/fonts/flag-icon-css/flag-icon.min.css') }}">
    <title>Hornet Real Estate</title>
    <link rel="icon" href="{{ url('app-assets/images/favicon/hornet_icon.png') }}" type="image/png" />

    <!-- Datatables-->
{{--    <link href="{{ url('app-assets/css/datatables/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />--}}
{{--    <link href="{{ url('app-assets/css/datatables/buttons.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
{{--    <link href="{{ url('app-assets/css/datatables/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
{{--    <link href="{{ url('app-assets/css/datatables/responsive.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
{{--    <link href="{{ url('app-assets/css/datatables/scroller.bootstrap.min.css') }}" rel="stylesheet" type="text/css" />--}}
</head>

<body>

@include('admin.layouts.header')
@include('admin.layouts.sidebar')
@yield('content')
@include('admin.layouts.footer')

<!-- loader -->
<script src="{{ url('app-assets/admin/vendor/jquery/jquery-3.3.1.min.js') }}"></script>
<!-- bootstap bundle js -->
<script src="{{ url('app-assets/admin/vendor/bootstrap/js/bootstrap.bundle.js') }}"></script>
<!-- slimscroll js -->
<script src="{{ url('app-assets/admin/vendor/slimscroll/jquery.slimscroll.js') }}"></script>
<!-- main js -->
<script src="{{ url('app-assets/admin/libs/js/main-js.js') }}"></script>
<!-- chart chartist js -->
<script src="{{ url('app-assets/admin/vendor/charts/chartist-bundle/chartist.min.js') }}"></script>
<!-- sparkline js -->
<script src="{{ url('app-assets/admin/vendor/charts/sparkline/jquery.sparkline.js') }}"></script>
<!-- morris js -->
<script src="{{ url('app-assets/admin/vendor/charts/morris-bundle/raphael.min.js') }}"></script>
<script src="{{ url('app-assets/admin/vendor/charts/morris-bundle/morris.js') }}"></script>
<!-- chart c3 js -->
<script src="{{ url('app-assets/admin/vendor/charts/c3charts/c3.min.js') }}"></script>
<script src="{{ url('app-assets/admin/vendor/charts/c3charts/d3-5.4.0.min.js') }}"></script>
<script src="{{ url('app-assets/admin/vendor/charts/c3charts/C3chartjs.js') }}"></script>
<script src="{{ url('app-assets/admin/libs/js/dashboard-ecommerce.js') }}"></script>
<script src="{{ url('app-assets/admin/libs/js/tinymce/tinymce.min.js') }}"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
{{--<script src="https://cloud.tinymce.com/stable/tinymce.min.js"></script>--}}

<!-- Datatables-->
{{--<script src="{{ url('app-assets/js/datatables/jquery.dataTables.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/dataTables.bootstrap.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/dataTables.buttons.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/buttons.bootstrap.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/jszip.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/pdfmake.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/vfs_fonts.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/buttons.html5.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/buttons.print.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/dataTables.fixedHeader.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/dataTables.keyTable.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/dataTables.responsive.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/responsive.bootstrap.min.js') }}"></script>--}}
{{--<script src="{{ url('app-assets/js/datatables/dataTables.scroller.min.js') }}"></script>--}}
@yield('scripts')

</body>


</html>
