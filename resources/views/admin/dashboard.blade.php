@extends('admin.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="dashboard-ecommerce">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Dashboard</h2>
                            <p class="pageheader-text">Nulla euismod urna eros, sit amet scelerisque torton lectus vel mauris facilisis faucibus at enim quis massa lobortis rutrum.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Users</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="ecommerce-widget">

                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <a href="{{ url('users/2') }}">
                                    <div class="card-body">
                                        <h5 class="text-muted">Total Contractor</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1">{{ $contractors}}</h1>
                                        </div>
                                        <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                                        </div>
                                    </div>
                                </a>
{{--                                <div id="sparkline-revenue"></div>--}}
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <a href="{{ url('users/3') }}">
                                    <div class="card-body">
                                        <h5 class="text-muted">Total Realtor</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1">{{ $realtors }}</h1>
                                        </div>
                                        <div class="metric-label d-inline-block float-right text-success font-weight-bold">
                                        </div>
                                    </div>
                                </a>
{{--                                <div id="sparkline-revenue2"></div>--}}
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <a href="{{ url('users/4') }}">
                                    <div class="card-body">
                                        <h5 class="text-muted">Total Wholesaler</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1">{{ $wholeselors }}</h1>
                                        </div>
                                        <div class="metric-label d-inline-block float-right text-primary font-weight-bold">
                                        </div>
                                    </div>
                                </a>
{{--                                <div id="sparkline-revenue3"></div>--}}
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <a href="{{ url('users/5') }}">
                                    <div class="card-body">
                                        <h5 class="text-muted">Total Accredited Investor</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1">{{ $accredited_investors }}</h1>
                                        </div>
                                        <div class="metric-label d-inline-block float-right text-secondary font-weight-bold">
                                        </div>
                                    </div>
                                </a>
{{--                                <div id="sparkline-revenue4"></div>--}}
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <a href="{{ url('users/6') }}">
                                    <div class="card-body">
                                        <h5 class="text-muted">Total Investor</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1">{{ $investors }}</h1>
                                        </div>
                                        <div class="metric-label d-inline-block float-right text-secondary font-weight-bold">
                                        </div>
                                    </div>
                                </a>
{{--                                <div id="sparkline-revenue4"></div>--}}
                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="card">
                                <a href="{{ url('users/7') }}">
                                    <div class="card-body">
                                        <h5 class="text-muted">Total Funding</h5>
                                        <div class="metric-value d-inline-block">
                                            <h1 class="mb-1"> {{ $fundings}}</h1>
                                        </div>
                                        <div class="metric-label d-inline-block float-right text-secondary font-weight-bold">
                                        </div>
                                    </div>
                                </a>
{{--                                <div id="sparkline-revenue4"></div>--}}
                            </div>
                        </div>
                    </div>

                
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
@endsection