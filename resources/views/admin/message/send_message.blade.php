@extends('admin.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="dashboard-ecommerce">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Send Message</h2>
                            <p class="pageheader-text">Nulla euismod urna eros, sit amet scelerisque torton lectus vel
                                mauris facilisis faucibus at enim quis massa lobortis rutrum.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">Message</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="influence-profile-content pills-regular">
                            <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-campaign-tab" data-toggle="pill" href="#pills-campaign" role="tab" aria-controls="pills-campaign" aria-selected="true">Messages</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-review-tab" data-toggle="pill" href="#pills-review" role="tab" aria-controls="pills-review" aria-selected="false">Send Messages</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-campaign" role="tabpanel" aria-labelledby="pills-campaign-tab">
                                    <div class="card">
                                        <h5 class="card-header">All Messages</h5>
                                        <div class="card-body">
                                            <div class="col-12 col-md-12 col-sm-12">
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" id="checkAll" class="custom-control-input"><span
                                                            class="custom-control-label">Check All</span>
                                                </label>
                                            </div>
                                            <div class="email-list">
                                                @foreach($receive_messages as $all_messages)
                                                    <div class="email-list-item email-list-item--unread">
                                                        <div class="email-list-actions">
                                                            <label class="custom-control custom-checkbox">
                                                                <input class="custom-control-input checkboxes" name="select_user" type="checkbox" id="one"><span class="custom-control-label"></span>
                                                            </label>
                                                        </div>
                                                        <div class="email-list-detail"><span class="date float-right">{{$all_messages->created_at}}</span><!-- <span class="from">Penelope Thornton</span> -->
                                                            <p class="msg">{!! $all_messages->reply !!}</p>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-12 col-sm-12">
                                            {{$receive_messages->render()}}
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="pills-review" role="tabpanel" aria-labelledby="pills-review-tab">
                                    <div class="card">
                                        <h5 class="card-header">Send Message</h5>
                                        <div class="card-body">
                                            <div class="col-md-12 text-center" id="error_of_checkbox"></div>
                                            <div class="col-12 col-md-12 col-sm-12">
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" id="checkAll" name="all_check" value="8"
                                                           class="custom-control-input"><span
                                                            class="custom-control-label">Check All</span>
                                                </label>
                                            </div>
                                            <div class="col-12 col-md-12 col-sm-12">
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" name="select_user" value="2"
                                                           class="custom-control-input"><span class="custom-control-label">Trusted
                                            Contractors</span>
                                                </label>
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" name="select_user" value="3"
                                                           class="custom-control-input"><span class="custom-control-label">Trusted Realtor</span>
                                                </label>
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" name="select_user" value="4"
                                                           class="custom-control-input"><span class="custom-control-label">Trusted
                                        Wholesaler</span>
                                                </label>
                                            </div>
                                            <div class="col-12 col-md-12 col-sm-12">
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" name="select_user" value="5"
                                                           class="custom-control-input"><span class="custom-control-label">Trusted Accredited
                                        Investor</span>
                                                </label>
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" name="select_user" value="6"
                                                           class="custom-control-input"><span class="custom-control-label">Trusted Investor</span>
                                                </label>
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" name="select_user" value="7"
                                                           class="custom-control-input"><span class="custom-control-label">Smart Funding</span>
                                                </label>
                                            </div>
                                            <div class="form-group row">
                                                <label for="message"
                                                       class="col-1 col-lg-1 col-md-12 col-form-label text-right">Message</label>
                                                <div class="col-111 col-lg-111 col-md-12">
                                    <textarea name="message" id="message" class="form-control"
                                              placeholder="Enter you message" required="">
                                    </textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12 col-lg-12">
                                                    <p class="text-right">
                                                        <button type="button" name="selected_user" class="btn btn-success"
                                                                onclick="selected_user()">Submit
                                                        </button>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function () {

        if ($("#message").length > 0) {
            tinymce.init({
                selector: "textarea#message",
                theme: "modern",
                height: 300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

            });
        }
    });

    function selected_user() {
        var userArray = [];
        var message = tinymce.get('message').getContent();
        $("input:checkbox[name=select_user]:checked").each(function () {
            userArray.push($(this).val());
        });

        if (userArray.length == "") {
            $("#error_of_checkbox").html("<p class = 'alert alert-danger'>Please check at least one checkbox</p>");
        } else if (message == "") {
            $("#error_of_checkbox").html("<p class = 'alert alert-danger'>Please write message</p>");
        } else {
            console.log(userArray);
            $.ajax({
                url: "{{url('message_insert')}}",
                type: 'POST',
                data: {'userArray': userArray, 'message': message, '_token': '{{csrf_token()}}'},
                success: function (data) {
                    tinyMCE.activeEditor.setContent('');
                    $("input:checkbox[name=select_user]").prop('checked', false);
                    $("input:checkbox[name=all_check]").prop('checked', false);
                    $("#error_of_checkbox").html("<p class = 'alert alert-success'>Message sent successfully</p>");
                }
            });
        }
    }

    $("#checkAll").click(function () {
        $("input:checkbox[name=select_user]").not(this).prop('checked', this.checked);
    });
</script>
@endsection