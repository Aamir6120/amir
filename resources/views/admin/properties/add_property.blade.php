@extends('admin.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="dashboard-ecommerce">
            <div class="container-fluid  dashboard-content">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Add New Property </h2>
                            <p class="pageheader-text"></p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="{{url('admin')}}" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item"><span class="breadcrumb-link">Add Property</span></li>                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
             
                    <div class="row">
                        <!-- ============================================================== -->
                        <!-- validation form -->
                        <!-- ============================================================== -->
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="card">
                                <h5 class="card-header">Property Detail</h5>
                                <div class="card-body">
                                    <form class="needs-validation" method="post" action="{{route('add_new_property')}}" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="inputWebSite" class="col-3 col-lg-2 col-form-label text-right">Picture</label>
                                            <div class="col-9 col-lg-10">
                                                <input type="file" name="picture"  class="form-control" required="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="inputEmail2" class="col-3 col-lg-2 col-form-label text-right">Title</label>
                                            <div class="col-9 col-lg-10">
                                                <input type="text" name="title" class="form-control" id="title" placeholder="title" required="">
                                            </div>
                                        </div>


                                        <div class="form-group row">
                                            <label for="status" class="col-3 col-lg-2 col-form-label text-right">Status</label>
                                            <div class="col-9 col-lg-10">
                                                <input type="text" name="status" class="form-control" id="status" placeholder="Status" required="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="category" class="col-3 col-lg-2 col-form-label text-right">Category</label>
                                            <div class="col-9 col-lg-10">
                                                <input type="text" name="category" class="form-control" id="category" placeholder="Category" required="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="price" class="col-3 col-lg-2 col-form-label text-right">Price</label>
                                            <div class="col-9 col-lg-10">
                                                <input type="text" name="price" class="form-control" id="price" placeholder="Price" required="">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="month" class="col-3 col-lg-2 col-form-label text-right">Month</label>
                                            <div class="col-9 col-lg-10">
                                                <input type="text" name="month" class="form-control" id="month" placeholder="Month" required="">
                                            </div>
                                        </div>
                                        
                                        <div class="row pt-2 pt-sm-5 mt-1">
                                            <div class="col-sm-6 pb-2 pb-sm-4 pb-lg-0 pr-0"></div>
                                            <div class="col-sm-6 pl-0">
                                                <p class="text-right">
                                                    <button type="submit" class="btn btn-space btn-primary">Submit</button>
                                                </p>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
@endsection

