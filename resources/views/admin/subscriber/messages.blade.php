@extends('admin.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="dashboard-ecommerce">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h2 class="pageheader-title">Send Message</h2>
                            <p class="pageheader-text">Nulla euismod urna eros, sit amet scelerisque torton lectus vel
                                mauris facilisis faucibus at enim quis massa lobortis rutrum.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Dashboard</a>
                                        </li>
                                        <li class="breadcrumb-item active" aria-current="page">Message</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader  -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- basic table  -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card">
                            <h5 class="card-header">Send Message</h5>
                            <div class="card-body">
                                <ul>
                                    <li class="d-flex">
                                        <div>
                                            <h4>Total Subscriber &nbsp;&nbsp;</h4>
                                        </div>
                                        <h4><span class="text-muted">12</span></h4>
                                    </li>
                                </ul>
                                <div class="form-group row">
                                    <label for="message"
                                           class="col-1 col-lg-1 col-md-12 col-form-label text-right">Message</label>
                                    <div class="col-111 col-lg-111 col-md-12">
                                    <textarea name="message" id="message" class="form-control"
                                              placeholder="Enter you message" required="">
                                    </textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-12 col-lg-12">
                                        <p class="text-right">
                                            <button type="button" name="selected_user" class="btn btn-success">Send</button>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <!-- end basic table  -->
                    <!-- ============================================================== -->
                </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            if ($("#message").length > 0) {
                tinymce.init({
                    selector: "textarea#message",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

                });
            }
        });
    </script>
@endsection