@extends('admin.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="influence-profile">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h3 class="mb-2">Subscribers </h3>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="url('admindashboard" class="breadcrumb-link">Dashboard</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Subscribers</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- campaign data -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <!-- ================================a============================== -->
                        <!-- campaign tab one -->
                        <!-- ============================================================== -->
                        <div class="influence-profile-content pills-regular">
                            <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">

                            </ul>
                            <div id="error_of_checkbox" class="text-center"></div>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-review" role="tabpanel" aria-labelledby="pills-review-tab">
                                    <div class="card">
                                        <h5 class="card-header">All Subscribers</h5>
                                        <div class="card-body">
                                            <div class="col-12 col-md-12 col-sm-12">
                                                <label class="custom-control custom-checkbox custom-control-inline">
                                                    <input type="checkbox" id="checkAll" class="custom-control-input"><span
                                                            class="custom-control-label">Check All</span>
                                                </label>
                                            </div>
                                            <div class="email-list">

                                                @foreach($subscribers as $subscribe)
                                                    <div class="email-list-item email-list-item--unread">
                                                        <div class="email-list-actions">
                                                            <label class="custom-control custom-checkbox">
                                                                <input class="custom-control-input checkboxes" name="select_user" type="checkbox" value="{{$subscribe->email}}" id="one"><span class="custom-control-label"></span>
                                                            </label>
                                                        </div>
                                                        <div class="email-list-detail"><span class="date float-right">{{$subscribe->created_at}}</span><!-- <span class="from">Penelope Thornton</span> -->
                                                            <p class="msg">{{$subscribe->email}}</p>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12 col-lg-12">
                                                    <p class="text-right">
                                                        <button type="button" name="" onclick="selected_user()" class="btn btn-info">Send Messages</button>
                                                    </p>
                                                </div>
                                            </div>
                                            {{$subscribers->render()}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end campaign tab one -->
                        <!-- ============================================================== -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- end campaign data -->
                    <!-- ============================================================== -->
                </div>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Message</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>

                            </div>
                            <div id="error_of_message" class="text-center"></div>
                            <div id="loading_message" class="text-center"></div>
                            <div class="modal-body">
                                <form class="needs-validation" method="" action="" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="message"
                                               class="col-1 col-lg-1 col-md-12 col-form-label text-right">Write Message</label>
                                        <div class="col-111 col-lg-111 col-md-12">
                                            <textarea name="message" id="message" class="form-control"
                                                      placeholder="Enter you message" required="">
                                            </textarea>
                                        </div>

                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="close_modal" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" onclick="send_email_message()" class="btn btn-success">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
@endsection
@section('scripts')
    <script>

        $(document).ready(function () {

            if ($("#message").length > 0) {
                tinymce.init({
                    selector: "textarea#message",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

                });
            }
        });

        $("#checkAll").click(function () {
            $("input:checkbox[name=select_user]").not(this).prop('checked', this.checked);
        });



        function selected_user() {

            
            var userArray = [];
            $("input:checkbox[name=select_user]:checked").each(function () {
                userArray.push($(this).val());
            });

            if (userArray.length == "") {
                $("#error_of_checkbox").html("<p class = 'alert alert-danger'>Please check at least one checkbox</p>");
            } else {
                $("#exampleModal").modal("show");
            }
            //     $("#error_of_checkbox").html("<p class = 'alert alert-danger'>Please write message</p>");
            // } else {
            //     console.log(userArray);
            //     $.ajax({
            //         url: "{{url('message_insert')}}",
            //         type: 'POST',
            //         data: {'userArray': userArray, 'message': message, '_token': '{{csrf_token()}}'},
            //         success: function (data) {
            //             tinyMCE.activeEditor.setContent('');
            //             $("input:checkbox[name=select_user]").prop('checked', false);
            //             $("input:checkbox[name=all_check]").prop('checked', false);
            //             $("#error_of_checkbox").html("<p class = 'alert alert-success'>Message sent successfully</p>");
            //         }
            //     });
            // }
        }

        function send_email_message(){
            var userArray = [];
            var message = tinymce.get('message').getContent();
            $("input:checkbox[name=select_user]:checked").each(function () {
                userArray.push($(this).val());
            });
            console.log(userArray);
            if (message == "") {
                $("#error_of_message").html("<p class = 'alert alert-danger'>Please write message</p>");
            } else {
                $("#loading_message").html("<span class='label label-success'>Please wait while email is sending...</span>");
                $.ajax({
                    url: "{{url('subscribers_message')}}",
                    type: 'POST',
                    data: {'userArray': userArray, 'message': message, '_token': '{{csrf_token()}}'},
                    success: function (data) {
                        $("#loading_message").html("");
                        tinyMCE.activeEditor.setContent('');
                        $("input:checkbox[name=select_user]").prop('checked', false);
                        $("input:checkbox[name=all_check]").prop('checked', false);
                        $("#exampleModal").modal("hide");
                        $("#error_of_checkbox").html("<p class = 'alert alert-success'>Message sent successfully</p>");
                    }
                });
            }
        } 






    </script>
@endsection