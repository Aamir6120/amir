@extends('layouts.app')
@section('content')
    <div class="hero-wrap" style="background-image: url({{ url('app-assets/images/bg_1.jpg') }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="#">Home</a></span> <span>Privacy & Policy</span></p>
                    <h1 class="mb-3 bread">Privacy & Policy</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section ftco-properties">
        <div class="container">
            <div class="row justify-content-center mb-2 pb-3">
                <div class="col-md-7 heading-section text-center ftco-animate">
                    <span class="subheading">ONLINE PRIVACY POLICY</span>
                    <h2 class="mb-4"> AGREEMENT</h2>
                </div>
            </div>
            <div class="row pb-1">
                <div class="col-md-12">
                    <p class="text-center">
                        Hornet Capital, (), is committed to keeping any and all personal information collected of those individuals
                        that visit our website and make use of our online facilities and services accurate, confidential, secure
                        and private. Our privacy policy has been designed and created to ensure those affiliated with Hornet
                        Capital of our commitment and realization of our obligation not only to meet but to exceed most existing
                        privacy standards.           THEREFORE, this Privacy Policy Agreement shall apply to Hornet Capital, and
                        thus it shall govern any and all data collection and usage thereof. Through the use of
                        www.hornetadvantage.com you are herein consenting to the following data procedures expressed
                        within this agreement.
                        <br>
                        It is highly recommended and suggested that you review the privacy policies and statements of any website
                        you choose to use or frequent as a means to better understand the way in which other websites garner, make
                        use of and share information collected.
                </div>
            </div>
            <section>
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-12 wrap-about ftco-animate">
                            <div class="heading-section heading-section-wo-line">
                                <h5 class="mb-2">Collection of Information
                                </h5>
                            </div>
                            <div class="pl-md-2 ml-md-2 mb-2">
                                <p>This website collects various types of information, such as:
                                    <br>
                                    -          Voluntarily provided information which may include your name, address, email
                                    address, billing and/or credit card information etc., which may be used when you purchase
                                    products and/or services and to deliver the services you have requested.
                                    <br>
                                    -          Information automatically collected when visiting our website, which may include
                                    cookies, third party tracking technologies and server logs.
                                    <br>
                                    Please rest assured that this site shall only collect personal information that you
                                    knowingly and willingly provide by way of surveys, completed membership forms, and emails.
                                    It is the intent of this site to use personal information only for the purpose for which it
                                    was requested and any additional uses specifically provided on this site.                                                                       Hornet Capitalmay have the occasion to collect non-personal anonymous demographic information, such as age, gender, household income, political affiliation, race and religion, as well as the type of browser you are using, IP address, type of operating system, at a later time, that will assist us in providing and maintaining superior quality service.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-12 wrap-about ftco-animate">
                            <div class="heading-section heading-section-wo-line">
                                <h5 class="mb-2">Use of Information Collected
                                </h5>
                            </div>
                            <div class="pl-md-2 ml-md-2 mb-2">
                                <p>Hornet Capitalmay collect and may make use of personal information to assist in the operation
                                    of our website and to ensure delivery of the services you need and request. At times, we
                                    may find it necessary to use personally identifiable information as a means to keep you
                                    informed of other possible products and/or services that may be available to you from
                                    www.hornetadvantage.com. Hornet Capital may also be in contact with you with regards to
                                    completing surveys and/or research questionnaires related to your opinion of current or
                                    potential future services that may be offered.
                                    <br>
                                    Hornet Capitaldoes not now, nor will it in the future, sell, rent or lease any of its
                                    customer lists and/or names to any third parties.
                                    <br>
                                    Hornet Capital, may feel it necessary, from time to time, to make contact with you on
                                    behalf of our other external business partners with regards to a potential new offer which
                                    may be of interest to you. If you consent or show interest in presented offers, then, at
                                    that time, specific identifiable information, such as name, email address and/or telephone
                                    number, may be shared with the third party.
                                    <br>
                                    Hornet Capitalmay find it beneficial to all our customers to share specific data with our
                                    trusted partners in an effort to conduct statistical analysis, provide you with email
                                    and/or postal mail, deliver support and/or arrange for deliveries to be made. Those third
                                    parties shall be strictly prohibited from making use of your personal information, other
                                    than to deliver those services which you requested, and as such they are required, in
                                    accordance with this agreement, to maintain the strictest of confidentiality with regards
                                    to all your information.
                                    <br>
                                    Hornet Capital may deem it necessary to follow websites and/or pages that their users may
                                    frequent in an effort to gleam what types of services and/or products may be the most popular
                                    to customers or the general public.
                                    <br>
                                    Hornet Capitalmay disclose your personal information, without any prior notice to you,
                                    unless required to do in accordance to applicable laws and/or in a good faith belief that
                                    such action is deemed necessary or required in an effort to:
                                    <br>
                                    -          Remain in conformance with any decrees, laws and/or statutes or in an effort
                                    to comply with any process which may be served upon Hornet Capitaland/or its website;
                                    <br>
                                    -          Maintain, safeguard and/or preserve all the rights and/or property ofH ornet
                                    Capital; and
                                    <br>
                                    -          Perform under demanding conditions in an effort to safeguard the personal
                                    safety of  users of www.hornetadvantage.comand/or the general public.
                                    <br>
                                    Hornet Capitalmay have the occasion to collect non-personal anonymous demographic
                                    information, such as age, gender, household income, political affiliation, race and
                                    religion, as well as the type of browser you are using, IP address, type of operating
                                    system, at a later time, that will assist us in providing and maintaining superior quality
                                    service.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-12 wrap-about ftco-animate">
                            <div class="heading-section heading-section-wo-line">
                                <h5 class="mb-2">Non-Marketing Purposes
                                </h5>
                            </div>
                            <div class="pl-md-2 ml-md-2 mb-2">
                                <p>Hornet Capitalgreatly respects your privacy.  We do maintain and reserve the right to
                                    contact you if needed for non-marketing purposes (such as bug alerts, security breaches,
                                    account issues, and/or changes in Hornet Capital products and services).  In certain
                                    circumstances, we may use our website, newspapers, or other public means to post a notice.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-12 wrap-about ftco-animate">
                            <div class="heading-section heading-section-wo-line">
                                <h5 class="mb-2">Children Under Age of 13
                                </h5>
                            </div>
                            <div class="pl-md-2 ml-md-2 mb-2">
                                <p>Hornet Capitaldoes not knowingly collect personal identifiable information from children
                                    under the age of thirteen (13) without verifiable parental consent. If it is determined
                                    that such information has been inadvertently collected on anyone under the age of thirteen
                                    (13), we shall immediately take the necessary steps to ensure that such information is
                                    deleted from our system's database. Anyone under the age of thirteen (13) must seek and
                                    obtain parent or guardian permission to use this website.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-12 wrap-about ftco-animate">
                            <div class="heading-section heading-section-wo-line">
                                <h5 class="mb-2">Unsubscribe or Opt-Out
                                </h5>
                            </div>
                            <div class="pl-md-2 ml-md-2 mb-2">
                                <p>All users and/or visitors to our website have the option to discontinue receiving
                                    communication from us and/or reserve the right to discontinue receiving communications
                                    by way of email or newsletters. To discontinue or unsubscribe to our website please send
                                    an email that you wish to unsubscribe to Info@Hornet.Capital . If you wish to unsubscribe
                                    or optout from any third party websites, you must go to that specific website to unsubscribe
                                    and/or opt-out. Hornet Capitalwill continue to adhere to the privacy policy described herein
                                    with respect to any personal information previously collected.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-12 wrap-about ftco-animate">
                            <div class="heading-section heading-section-wo-line">
                                <h5 class="mb-2">Links to Other Web Sites
                                </h5>
                            </div>
                            <div class="pl-md-2 ml-md-2 mb-2">
                                <p>Our website does contain links to affiliate and other websites. Hornet Capitaldoes not
                                    claim nor accept responsibility for any privacy policies, practices and/or procedures
                                    of other such websites. Therefore, we encourage all users and visitors to be aware when
                                    they leave our website and to read the privacy statements of each and every website that
                                    collects personally identifiable information. The aforementioned Privacy Policy Agreement
                                    applies only and solely to the information collected by our website.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-12 wrap-about ftco-animate">
                            <div class="heading-section heading-section-wo-line">
                                <h5 class="mb-2">Security
                                </h5>
                            </div>
                            <div class="pl-md-2 ml-md-2 mb-2">
                                <p>Hornet Capital shall endeavor and shall take every precaution to maintain adequate
                                    physical, procedural and technical security with respect to its offices and information
                                    storage facilities so as to prevent any loss, misuse, unauthorized access, disclosure or
                                    modification of the user's personal information under our control.
                                    <br>
                                    The company also uses Secure Socket Layer (SSL) for authentication and private
                                    communications in an effort to build users' trust and confidence in the internet and
                                    website use by providing simple and secure access and communication of credit card and
                                    personal information.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-12 wrap-about ftco-animate">
                            <div class="heading-section heading-section-wo-line">
                                <h5 class="mb-2">Changes to Privacy Policy Agreement
                                </h5>
                            </div>
                            <div class="pl-md-2 ml-md-2 mb-2">
                                <p>Hornet Capitalreserves the right to update and/or change the terms of our privacy policy,
                                    and as such we will post those changes to our website, so that our users and/or visitors
                                    are always aware of the type of information we collect, how it will be used, and under what
                                    circumstances, if any, we may disclose such information. If at any point in time Hornet
                                    Capital decides to make use of any personally identifiable information on file, in a manner
                                    vastly different from that which was stated when this information was initially collected,
                                    the user or users shall be promptly notified by email. Users at that time shall have the
                                    option as to whether or not to permit the use of their information in this separate manner.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-12 wrap-about ftco-animate">
                            <div class="heading-section heading-section-wo-line">
                                <h5 class="mb-2">Acceptance of Terms
                                </h5>
                            </div>
                            <div class="pl-md-2 ml-md-2 mb-2">
                                <p>Through the use of this website, you are hereby accepting the terms and conditions stipulated
                                    within the aforementioned Privacy Policy Agreement. If you are not in agreement with our
                                    terms and conditions, then you should refrain from further use of our sites. In addition,
                                    your continued use of our website following the posting of any updates or changes to our
                                    terms and conditions shall mean that you are in agreement and acceptance of such changes.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div class="container">
                    <div class="row no-gutters">
                        <div class="col-md-12 wrap-about ftco-animate">
                            <div class="heading-section heading-section-wo-line">
                                <h5 class="mb-2">How to Contact Us
                                </h5>
                            </div>
                            <div class="pl-md-2 ml-md-2 mb-2">
                                <p>If you have any questions or concerns regarding the Privacy Policy Agreement related to
                                    our website, please feel free to contact us at the following email, telephone number or
                                    mailing address.
                                    <br>
                                    Email: Info@Hornet.Capital
                                    <br>
                                    Telephone Number: (512) 580-7002
                                    <br>
                                    Mailing Address:      Hornet Capital           Austin, Texas
                                    May 21, 2019
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>
@endsection