@extends('users.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="influence-profile">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    @if(Auth::user()->role == 6)
                        <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-10">
                            <div class="page-header">
                                <h3 class="mb-2">Dashboard </h3>
                                <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Users</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Overview</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-2">
                            <a href="https://secure.hornet.capital/home" target="_blank"><button type="button" class="btn btn-info" style="float: right;">Invest Now</button></a>
                        </div>
                    @else
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                            <div class="page-header">
                                <h3 class="mb-2">Dashboard </h3>
                                <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                                <div class="page-breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Users</a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Overview</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- content -->
                <!-- ============================================================== -->
{{--                <div class="row">--}}
{{--                    <!-- ============================================================== -->--}}
{{--                    <!-- campaign data -->--}}
{{--                    <!-- ============================================================== -->--}}
{{--                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">--}}
{{--                        <!-- ================================a============================== -->--}}
{{--                        <!-- campaign tab one -->--}}
{{--                        <!-- ============================================================== -->--}}
{{--                        <div class="influence-profile-content pills-regular">--}}
{{--                            <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link active" id="pills-campaign-tab" data-toggle="pill" href="#pills-campaign" role="tab" aria-controls="pills-campaign" aria-selected="true">Title</a>--}}
{{--                                </li>--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link" id="pills-review-tab" data-toggle="pill" href="#pills-review" role="tab" aria-controls="pills-review" aria-selected="false">Messages</a>--}}
{{--                                </li>--}}
{{--                                <li class="nav-item">--}}
{{--                                    <a class="nav-link" id="pills-msg-tab" data-toggle="pill" href="#pills-msg" role="tab" aria-controls="pills-msg" aria-selected="false">Send Messages</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <div class="tab-content" id="pills-tabContent">--}}
{{--                                <div class="tab-pane fade show active" id="pills-campaign" role="tabpanel" aria-labelledby="pills-campaign-tab">--}}
{{--                                    <div class="row">--}}
{{--                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">--}}
{{--                                            <div class="section-block">--}}
{{--                                                <h3 class="section-title">My Task Status</h3>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">--}}
{{--                                            <div class="card">--}}
{{--                                                <div class="card-body">--}}
{{--                                                    <h1 class="mb-1">0</h1>--}}
{{--                                                    <p>Task One</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">--}}
{{--                                            <div class="card">--}}
{{--                                                <div class="card-body">--}}
{{--                                                    <h1 class="mb-1">0</h1>--}}
{{--                                                    <p>Task Two</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">--}}
{{--                                            <div class="card">--}}
{{--                                                <div class="card-body">--}}
{{--                                                    <h1 class="mb-1">0</h1>--}}
{{--                                                    <p>Task Three</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">--}}
{{--                                            <div class="card">--}}
{{--                                                <div class="card-body">--}}
{{--                                                    <h1 class="mb-1">0</h1>--}}
{{--                                                    <p>Task Four</p>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="tab-pane fade" id="pills-review" role="tabpanel" aria-labelledby="pills-review-tab">--}}
{{--                                    <div class="card">--}}
{{--                                        <h5 class="card-header">All Messages</h5>--}}
{{--                                        <div class="card-body">--}}
{{--                                            <div class="review-block">--}}
{{--                                                <p class="review-text font-italic m-0">“Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.”</p>--}}
{{--                                                <div class="rating-star mb-4">--}}
{{--                                                    <i class="fa fa-fw fa-star"></i>--}}
{{--                                                    <i class="fa fa-fw fa-star"></i>--}}
{{--                                                    <i class="fa fa-fw fa-star"></i>--}}
{{--                                                    <i class="fa fa-fw fa-star"></i>--}}
{{--                                                    <i class="fa fa-fw fa-star"></i>--}}
{{--                                                </div>--}}
{{--                                                <span class="text-dark font-weight-bold">Name</span><small class="text-mute"> (Title)</small>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <nav aria-label="Page navigation example">--}}
{{--                                        <ul class="pagination">--}}
{{--                                            <li class="page-item"><a class="page-link" href="#">Previous</a></li>--}}
{{--                                            <li class="page-item"><a class="page-link" href="#">1</a></li>--}}
{{--                                            <li class="page-item active"><a class="page-link " href="#">2</a></li>--}}
{{--                                            <li class="page-item"><a class="page-link" href="#">3</a></li>--}}
{{--                                            <li class="page-item"><a class="page-link" href="#">Next</a></li>--}}
{{--                                        </ul>--}}
{{--                                    </nav>--}}
{{--                                </div>--}}
{{--                                <div class="tab-pane fade" id="pills-msg" role="tabpanel" aria-labelledby="pills-msg-tab">--}}
{{--                                    <div class="card">--}}
{{--                                        <h5 class="card-header">Send Messages</h5>--}}
{{--                                        <div class="card-body">--}}
{{--                                            <form>--}}
{{--                                                <div class="row">--}}
{{--                                                    <div class="offset-xl-3 col-xl-6 offset-lg-3 col-lg-3 col-md-12 col-sm-12 col-12 p-4">--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label for="name">Your Name</label>--}}
{{--                                                            <input type="text" class="form-control form-control-lg" id="name" placeholder="">--}}
{{--                                                        </div>--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label for="email">Your Email</label>--}}
{{--                                                            <input type="email" class="form-control form-control-lg" id="email" placeholder="">--}}
{{--                                                        </div>--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label for="subject">Subject</label>--}}
{{--                                                            <input type="text" class="form-control form-control-lg" id="subject" placeholder="">--}}
{{--                                                        </div>--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label for="messages">Messgaes</label>--}}
{{--                                                            <textarea class="form-control" id="messages" rows="3"></textarea>--}}
{{--                                                        </div>--}}
{{--                                                        <button type="submit" class="btn btn-primary float-right">Send Message</button>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </form>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- ============================================================== -->--}}
{{--                        <!-- end campaign tab one -->--}}
{{--                        <!-- ============================================================== -->--}}
{{--                    </div>--}}
{{--                    <!-- ============================================================== -->--}}
{{--                    <!-- end campaign data -->--}}
{{--                    <!-- ============================================================== -->--}}
{{--                </div>--}}
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
@endsection