@extends('users.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="influence-profile">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-10 col-lg-10 col-md-10 col-sm-12 col-10">
                        <div class="page-header">
                            <h3 class="mb-2">Profile </h3>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Users</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-2 col-md-2 col-sm-12 col-2">
                        <a href="{{ route('messages') }}"><button type="button" class="btn btn-info" data-toggle="modal" data-target="#exampleModal" style="float: right;">Opportunity</button></a>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- profile -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <!-- ============================================================== -->
                        <!-- card profile -->
                        <!-- ============================================================== -->
                        <div class="card">
                            <div class="card-body">
                                <div class="user-avatar text-center d-block">
                                    <img src="{{ url('app-assets/users/images/avatar-1.jpg') }}" alt="User Avatar" class="rounded-circle user-avatar-xxl">
                                </div>
                                <div class="text-center">
                                    <h2 class="font-24 mb-0">{{Auth::user()->email}}</h2>
                                    <p>{{Auth::user()->role}}</p>
                                </div>
                            </div>
                            <div class="card-body border-top">
                                <h3 class="font-16">Basic Information</h3>
                                <div class="">
                                    <ul class="list-unstyled mb-0">
                                        <li class="mb-2"><i class="fas fa-fw fa-envelope mr-2"></i>xyz@gmail.com</li>
                                        <li class="mb-0"><i class="fas fa-fw fa-phone mr-2"></i>{{Auth::user()->phone}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end card profile -->
                        <!-- ============================================================== -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- end profile -->
                    <!-- ============================================================== -->
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
@endsection