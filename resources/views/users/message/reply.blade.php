@extends('users.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="influence-profile">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h3 class="mb-2">Dashboard </h3>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Users</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Overview</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- campaign data -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <!-- ================================a============================== -->
                        <!-- campaign tab one -->
                        <!-- ============================================================== -->
                        <div class="influence-profile-content pills-regular">
                            <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-review-tab" data-toggle="pill" href="#pills-review" role="tab" aria-controls="pills-review" aria-selected="false">Reply</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-review" role="tabpanel" aria-labelledby="pills-review-tab">
                                    <div class="card">
                                        <h5 class="card-header">Message</h5>

                                        @if(session('status'))
                                            <div class="col-sm-12"><h3 class="alert alert-success">{{session('status')}}</h3></div>
                                        @endif
                                        <div class="card-body">
                                            <div class="review-block">
                                                <p class="review-text font-italic m-0">{!! $message->message !!}</p>
                                                <br>
                                                <!-- <span class="text-dark font-weight-bold">abc@gmail.com</span><small class="text-mute"> (Admin)</small> -->
                                            </div>
                                            <br>
                                            <form method="post" action="{{route('message_reply')}}" enctype="multipart/form-data"> 
                                                @csrf
                                                <div class="form-group row">
                                                    <label class="col-12 col-lg-12 col-form-label">Reply</label>
                                                    <div class="col-12 col-lg-12">
                                                        <textarea name="detail" id="modern_textarea" class="form-control" placeholder="Detail" required=""><p></p></textarea>
                                                    </div>

                                                    @if ($errors->has('detail'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('detail') }}</strong>
                                                        </span>
                                                    @endif
                                                    <input type="hidden" name="message_id" value="{{$message->id}}">
                                                </div>
                                                <div class="col-sm-12 pl-0">
                                                    <p class="text-right">
                                                        <button type="submit" class="btn btn-space btn-success">Send</button>
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end campaign tab one -->
                        <!-- ============================================================== -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- end campaign data -->
                    <!-- ============================================================== -->
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
@endsection
@section('scripts')
    <script>

        $(document).ready(function() {

            if ($("#modern_textarea").length > 0) {
                tinymce.init({
                    selector: "textarea#modern_textarea",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

                });
            }
        });

    </script>

@endsection