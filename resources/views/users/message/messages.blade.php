@extends('users.layouts.app')
@section('content')
    <div class="dashboard-wrapper">
        <div class="influence-profile">
            <div class="container-fluid dashboard-content ">
                <!-- ============================================================== -->
                <!-- pageheader -->
                <!-- ============================================================== -->

                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-header">
                            <h3 class="mb-2">Dashboard </h3>
                            <p class="pageheader-text">Proin placerat ante duiullam scelerisque a velit ac porta, fusce sit amet vestibulum mi. Morbi lobortis pulvinar quam.</p>
                            <div class="page-breadcrumb">
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">Users</a></li>
                                        <li class="breadcrumb-item active" aria-current="page">Overview</li>
                                    </ol>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- end pageheader -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- content -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- ============================================================== -->
                    <!-- campaign data -->
                    <!-- ============================================================== -->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <!-- ================================a============================== -->
                        <!-- campaign tab one -->
                        <!-- ============================================================== -->
                        <div class="influence-profile-content pills-regular">
                            <ul class="nav nav-pills mb-3 nav-justified" id="pills-tab" role="tablist">
                                
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-review" role="tabpanel" aria-labelledby="pills-review-tab">
                                    <div class="card">
                                        <h5 class="card-header">All Messages</h5>
                                        <div class="card-body">
                                            <div class="email-list">
                                                @foreach($messages as $message)
                                                    <div class="email-list-item email-list-item--unread">
                                                        <div class="email-list-actions">
                                                            <!-- <label class="custom-control custom-checkbox">
                                                                <input class="custom-control-input checkboxes" type="checkbox" value="1" id="one"><span class="custom-control-label"></span>
                                                            </label> --><a class="favorite active" href="#"><span><i class="fas fa-star"></i></span></a>
                                                        </div>
                                                        <div class="email-list-detail"><span class="date float-right"><span class="icon"><a
                                                                            href='{{ url("reply/$message->id") }}'><i class="fas fa-reply"></i></a></span>{{ $message->created_at}}</span><!-- <span class="from">Penelope Thornton</span> -->
                                                            <p class="msg">{!! $message->message !!}</p>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    {{$messages->render()}}
                                </div>
                            </div>
                        </div>
                        <!-- ============================================================== -->
                        <!-- end campaign tab one -->
                        <!-- ============================================================== -->
                    </div>
                    <!-- ============================================================== -->
                    <!-- end campaign data -->
                    <!-- ============================================================== -->
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- end content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- end footer -->
        <!-- ============================================================== -->
    </div>
@endsection