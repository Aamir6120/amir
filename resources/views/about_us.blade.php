@extends('layouts.app')
@section('content')
<div class="hero-wrap" style="background-image: url({{ url('app-assets/images/bg_1.jpg') }});">
    <div class="overlay"></div>
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 ftco-animate text-center">
                <p class="breadcrumbs"><span class="mr-2"><a href="#">Home</a></span> <span>About</span></p>
                <h1 class="mb-3 bread">About</h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section ftco-properties">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <span class="subheading">About Us</span>
                <h2 class="mb-4">Hornet Capital</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-center">HORNET CAPITAL is committed to improving the journey of ALL real estate entrepreneurs; rehabbers &
                    developers, new & experienced, cash investors & private money lenders alike.  We currently offer hard
                    money loans and partnerships on Austin rehabs & developments, foreclosure consulting, construction &
                    contractor referrals, discounted Realtor services, and personalized real estate education.  We provide
                    awesome returns for cash investors & private money lenders who are looking for mailbox money.  Yes, we
                    are different!  Ask us why.</p>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section-parallax">
    <div class="parallax-img d-flex align-items-center">
        <div class="container">
            <div class="row d-flex justify-content-center">
                <div class="col-md-12 text-center heading-section heading-section-white ftco-animate">
                    <h2>Management Team</h2>
                    <p>The entire management team has a rich history in real estate investments from every stage of the process:
                        Development, Raising Capital, Project Management, Construction, Negotiations, Deal Validation, Private
                        Lending and more.</p>
{{--                    <div class="row d-flex justify-content-center mt-5">--}}
{{--                        <div class="col-md-8">--}}
{{--                            <form action="#" class="subscribe-form">--}}
{{--                                <div class="form-group d-flex">--}}
{{--                                    <input type="text" class="form-control" placeholder="Enter email address">--}}
{{--                                    <input type="submit" value="Subscribe" class="submit px-3">--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftc-no-pb">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{ url('app-assets/images/ceo_picture.png') }});">
{{--                <a href="https://vimeo.com/45830194" class="icon popup-vimeo d-flex justify-content-center align-items-center">--}}
{{--                    <span class="icon-play"></span>--}}
{{--                </a>--}}
            </div>
            <div class="col-md-7 wrap-about pb-md-5 ftco-animate">
                <div class="heading-section heading-section-wo-line mb-5 pl-md-5">
                    <div class="pl-md-5 ml-md-5">
                        <span class="subheading">CEO/Founder</span>
                        <h2 class="mb-4">Brian Whitten</h2>
                    </div>
                </div>
                <div class="pl-md-5 ml-md-5 mb-5">
                    <p>Brian Whitten has been an active real estate investor for over 15 years.  A graduate from the
                        United States Naval Academy in Annapolis, MD, Brian traveled the world as a fighter pilot.
                        During this time, Brian acquired rental properties across the United States.
                        <br><br>

                        When he was not launching his F- 18 Super Hornet off the front of an aircraft carrier protecting
                        our country, he was pursuing his other passion – real estate. In 2008, he became a licensed Realtor
                        and quickly realized the true potential of investing in the housing market while he was representing
                        clients.</p>

                    <p id="paragraph_1" hidden>Starting in 2009, he partnered with other investors to find fix- and -flip properties. He assembled
                        a crew, tackled the projects one – by – one, and turned a profit for both him and his investors all
                        during his off-duty time from the Navy.
                        <br><br>

                        In 2012, Brian stepped back from active duty to pursue real estate. He moved to Austin
                        and began to network with local investors. A year later, he formed Tally Two Investment Group.
                        His company has continued to provide above-market returns for his investors while flipping properties,
                        providing hard money loans, and partnering with other real estate investors.
                        <br><br>

                        ​Brian is a seasoned investor with a great reputation and presence in Austin, TX. He
                        has capitalized on several unique opportunities with his extensive knowledge of real estate
                        law and creativity.
                        <br><br>

                        Brian lives with his wife and two boys in Austin. They love spending time on the lake, smoking BBQ,
                        and cheering on their Longhorn football team. Commander Whitten stays active in the Naval Reserves
                        and combines his love of flying with a desire to work harder and smarter to make his new endeavor
                        successful for himself and his investors.</p>
                    <p><div class="btn btn-primary small" id="learn_more1">Learn More <span class="ion-ios-arrow-forward"></span></div></p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section ftc-no-pb">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{ url('app-assets/images/management.png') }});">
                {{--                <a href="https://vimeo.com/45830194" class="icon popup-vimeo d-flex justify-content-center align-items-center">--}}
                {{--                    <span class="icon-play"></span>--}}
                {{--                </a>--}}
            </div>
            <div class="col-md-7 wrap-about pb-md-5 ftco-animate">
                <div class="heading-section heading-section-wo-line mb-5 pl-md-5">
                    <div class="pl-md-5 ml-md-5">
                        <span class="subheading">Management</span>
                        <h2 class="mb-4">Jordon Olson</h2>
                    </div>
                </div>
                <div class="pl-md-5 ml-md-5 mb-5">
                    <p>Jordon has been in the real estate business since the early nineties throughout Canada and the
                        United States. His diverse experience includes residential & commercial, flips, rentals, raw land,
                        and developments.
                        <br><br>

                        Jordon has been successful at creating several real estate teams throughout the United States
                        in order to create a personal passive income rental portfolio. He has also diversified his portfolio
                        by also investing in mobile homes, creating notes, and lending private and hard money.</p>


                    <p id="paragraph_2" hidden>Jordon lives with his wife and two kids in Austin, TX. We are excited to be able to add someone
                        with his diverse background to our team!</p>
                    <p><div class="btn btn-primary small" id="learn_more2">Learn More <span class="ion-ios-arrow-forward"></span></div></p>
                </div>
            </div>
        </div>
    </div>
</section>

{{--<section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url({{ url('app-assets/images/bg_1.jpg') }});">--}}
{{--    <div class="container">--}}
{{--        <div class="row justify-content-center mb-3 pb-3">--}}
{{--            <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">--}}
{{--                <h2 class="mb-4">Some fun facts</h2>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-10">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">--}}
{{--                        <div class="block-18 text-center">--}}
{{--                            <div class="text">--}}
{{--                                <strong class="number" data-number="9000">0</strong>--}}
{{--                                <span>Happy Customers</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">--}}
{{--                        <div class="block-18 text-center">--}}
{{--                            <div class="text">--}}
{{--                                <strong class="number" data-number="10000">0</strong>--}}
{{--                                <span>Properties</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">--}}
{{--                        <div class="block-18 text-center">--}}
{{--                            <div class="text">--}}
{{--                                <strong class="number" data-number="1000">0</strong>--}}
{{--                                <span>Agents</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">--}}
{{--                        <div class="block-18 text-center">--}}
{{--                            <div class="text">--}}
{{--                                <strong class="number" data-number="100">0</strong>--}}
{{--                                <span>Awards</span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}


{{--<section class="ftco-section testimony-section bg-light">--}}
{{--    <div class="container">--}}
{{--        <div class="row justify-content-center">--}}
{{--            <div class="col-md-8 ftco-animate">--}}
{{--                <div class="row ftco-animate">--}}
{{--                    <div class="col-md-12">--}}
{{--                        <div class="carousel-testimony owl-carousel ftco-owl">--}}
{{--                            <div class="item">--}}
{{--                                <div class="testimony-wrap py-4 pb-5">--}}
{{--                                    <div class="user-img mb-4" style="background-image: url({{ url('app-assets/images/person_1.jpg') }})">--}}
{{--		                    <span class="quote d-flex align-items-center justify-content-center">--}}
{{--		                      <i class="icon-quote-left"></i>--}}
{{--		                    </span>--}}
{{--                                    </div>--}}
{{--                                    <div class="text text-center">--}}
{{--                                        <p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>--}}
{{--                                        <p class="name">Roger Scott</p>--}}
{{--                                        <span class="position">Clients</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <div class="testimony-wrap py-4 pb-5">--}}
{{--                                    <div class="user-img mb-4" style="background-image: url({{ url('app-assets/images/person_2.jpg') }})">--}}
{{--		                    <span class="quote d-flex align-items-center justify-content-center">--}}
{{--		                      <i class="icon-quote-left"></i>--}}
{{--		                    </span>--}}
{{--                                    </div>--}}
{{--                                    <div class="text text-center">--}}
{{--                                        <p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>--}}
{{--                                        <p class="name">Roger Scott</p>--}}
{{--                                        <span class="position">Agent</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <div class="testimony-wrap py-4 pb-5">--}}
{{--                                    <div class="user-img mb-4" style="background-image: url({{ url('app-assets/images/person_3.jpg') }})">--}}
{{--		                    <span class="quote d-flex align-items-center justify-content-center">--}}
{{--		                      <i class="icon-quote-left"></i>--}}
{{--		                    </span>--}}
{{--                                    </div>--}}
{{--                                    <div class="text text-center">--}}
{{--                                        <p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>--}}
{{--                                        <p class="name">Roger Scott</p>--}}
{{--                                        <span class="position">Client</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <div class="testimony-wrap py-4 pb-5">--}}
{{--                                    <div class="user-img mb-4" style="background-image: url({{ url('app-assets/images/person_1.jpg') }})">--}}
{{--		                    <span class="quote d-flex align-items-center justify-content-center">--}}
{{--		                      <i class="icon-quote-left"></i>--}}
{{--		                    </span>--}}
{{--                                    </div>--}}
{{--                                    <div class="text text-center">--}}
{{--                                        <p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>--}}
{{--                                        <p class="name">Roger Scott</p>--}}
{{--                                        <span class="position">Client</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="item">--}}
{{--                                <div class="testimony-wrap py-4 pb-5">--}}
{{--                                    <div class="user-img mb-4" style="background-image: url({{ url('app-assets/images/person_1.jpg') }})">--}}
{{--		                    <span class="quote d-flex align-items-center justify-content-center">--}}
{{--		                      <i class="icon-quote-left"></i>--}}
{{--		                    </span>--}}
{{--                                    </div>--}}
{{--                                    <div class="text text-center">--}}
{{--                                        <p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>--}}
{{--                                        <p class="name">Roger Scott</p>--}}
{{--                                        <span class="position">Client</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
{{--<section class="ftco-section-parallax">--}}
{{--    <div class="parallax-img d-flex align-items-center">--}}
{{--        <div class="container">--}}
{{--            <div class="row d-flex justify-content-center">--}}
{{--                <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">--}}
{{--                    <h2>Subcribe to our Newsletter</h2>--}}
{{--                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>--}}
{{--                    <div class="row d-flex justify-content-center mt-5">--}}
{{--                        <div class="col-md-8">--}}
{{--                            <form action="#" class="subscribe-form">--}}
{{--                                <div class="form-group d-flex">--}}
{{--                                    <input type="text" class="form-control" placeholder="Enter email address">--}}
{{--                                    <input type="submit" value="Subscribe" class="submit px-3">--}}
{{--                                </div>--}}
{{--                            </form>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
@endsection
@section('scripts')

        $(document).ready(function() {
            var learn_more1 = 1;
            var learn_more2 = 1;
            $("#learn_more1").click(function() {
                if(learn_more1 == 1)
                {
                    $("#paragraph_1").removeAttr("hidden");
                    learn_more1++;
                }
                else
                {
                    $("#paragraph_1").attr("hidden", "hidden");
                    learn_more1--;
                }
            });
            $("#learn_more2").click(function() {

                if(learn_more2 == 1)
                {
                    $("#paragraph_2").removeAttr("hidden");
                    learn_more2++;
                }
                else
                {
                    $("#paragraph_2").attr("hidden", "hidden");
                    learn_more2--;
                }
            });
        });

@endsection