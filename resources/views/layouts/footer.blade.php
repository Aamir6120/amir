<footer class="ftco-footer ftco-bg-dark ftco-section">
    <div class="container">
        <div class="row mb-5">
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Hornet Capital</h2>
                    <p>The Hornet Advantage is designed to protect our investors with trusted agents, contractors, partners,
                        and a success team for every project. By connecting trusted partners we help reduce the liability of
                        the investment, and increase the overall performance of every project..  </p>
                    <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                        <li class="ftco-animate"><a href="{{ url('https://www.facebook.com/hornetcapital/')  }}"><span class="icon-facebook"></span></a></li>
                        <li class="ftco-animate"><a href="{{ url('https://twitter.com/Hornet_Capital')  }}"><span class="icon-twitter"></span></a></li>
                        <li class="ftco-animate"><a href="{{ url('https://www.youtube.com/channel/UC32lKCt72VDjTV4m6MYf-Tg')  }}"><span class="icon-youtube-play"></span></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4 ml-md-5">
                    <h2 class="ftco-heading-2">Links</h2>
                    <ul class="list-unstyled">
                        <li><a href="{{ url('/termsofservice') }}" class="py-2 d-block">Terms of Service</a></li>
                        <li><a href="{{ url('/privacypolicy') }}" class="py-2 d-block">Privacy & Policy</a></li>
                        <li><a href="{{ url('/about')  }}" class="py-2 d-block">About Us</a></li>
                        <li><a href="{{ url('/contact')  }}" class="py-2 d-block">Contact Us</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md">
                <div class="ftco-footer-widget mb-4">
                    <h2 class="ftco-heading-2">Have a Questions?</h2>
                    <div class="block-23 mb-3">
                        <ul>
                            <li><span class="icon icon-map-marker"></span><span class="text">Austin, TX </span></li>
                            <li><a href="#"><span class="icon icon-phone"></span><span class="text">(512) 580-7002 </span></a></li>
                            <li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@hornet.capital </span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">

                <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright <a href="{{ url('/') }}"><span class="text">Hornet Capital</span></a> All Rights Reserved
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            </div>
        </div>
    </div>
</footer>
@if(!empty($route))
    @else
    @section('scripts')

    var timeout = '{{ $all_transition[0]->time_out }}';
            var speed = '{{ $all_transition[0]->speed }}';
            $(document).ready(function(){

                $('.home-slider').owlCarousel({
                    loop:true,
                    autoplay:true,
                    autoplayTimeout:timeout,
                    smartSpeed: speed,
                    margin:0,
                    animateOut: 'fadeOut',
                    animateIn: 'fadeIn',
                    nav:false,
                    dots: false,
                    autoplayHoverPause: false,
                    items: 1,
                    navText : ["<span class='ion-md-arrow-back'></span>","<span class='ion-chevron-right'></span>"],
                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        1000:{
                            items:1
                        }
                    }
                });
            });

    function addsubscribe()
    {
        $("#sub_message").attr('hidden');
        var sub_email = $("#sub_email").val();

        if(sub_email != "")
        {
            $.ajax({
                url: "{{url('add_subscriber')}}",
                type: 'POST',
                data: { 'email'  : sub_email, '_token': '{{csrf_token()}}'},
                success: function(data)
                {
                    alerts(data);
                }
            });
        }
        else
        {
{{--            $("#sub_message").html("Please enter your email");--}}
            $(".my_alert p").text("Please enter your email");
            $(".my_alert").removeAttr("hidden").addClass('alert-danger');
        }
    }

    function alerts(data)
    {
        $(".my_alert").attr("hidden", true).removeClass('alert-danger').removeClass('alert-success');

        if(data.success == false)
        {
            $(".my_alert p").text(data.message);
            $(".my_alert").removeAttr("hidden").addClass('alert-danger');
        }
        else
        {
            $(".my_alert p").text(data.message);
            $(".my_alert").removeAttr("hidden").addClass('alert-success');
        }
    }


    @endsection

@endif