
<div class="top">
    <div class="container">
        <div class="row d-flex align-items-center">
            <div class="col">
                <p class="social d-flex">
                    <a href="{{ url('https://www.facebook.com/hornetcapital/')  }}"><span class="icon-facebook"></span></a>
                    <a href="{{ url('https://twitter.com/Hornet_Capital')  }}"><span class="icon-twitter"></span></a>
                    <a href="{{ url('https://www.youtube.com/channel/UC32lKCt72VDjTV4m6MYf-Tg')  }}"><span class="icon-youtube-play"></span></a>
                </p>
            </div>
            <div class="col d-flex justify-content-end">
                <p class="num"><span class="icon-phone"></span> (512) 580-7002</p>
            </div>
        </div>
    </div>
</div>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/')  }}"><img src="{{ url('app-assets/images/logo/hornet cap_edited.png')  }}" height="50px" width="150px" alt="" /></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="{{ url('/')  }}" class="nav-link">Home</a></li>
{{--                <li class="nav-item"><a href="#" class="nav-link">Property</a></li>--}}
{{--                <li class="nav-item"><a href="{{ url('/termsofservice')  }}" class="nav-link">Terms of Service</a></li>--}}
{{--                <li class="nav-item"><a href="{{ url('/privacypolicy')  }}" class="nav-link">Privacy Policy</a></li>--}}
                <li class="nav-item"><a href="{{ url('/about')  }}" class="nav-link">About</a></li>
                <li class="nav-item"><a href="{{ url('/education')  }}" class="nav-link">Education</a></li>
                <li class="nav-item"><a href="{{ url('/contact')  }}" class="nav-link">Contact</a></li>
                <li class="nav-item"><a href="{{ url('login')  }}" class="nav-link">Login</a></li>
{{--                <li class="nav-item cta"><a href="{{ url('login')  }}" class="nav-link ml-lg-2"><span class="icon-user"></span> Log-In</a></li>--}}
{{--                <li class="nav-item cta"><a href="{{ url('register')  }}" class="nav-link ml-lg-2"><span class="icon-pencil"></span> Register</a></li>--}}
                <li class="nav-item cta cta"><a href="{{ url('register')  }}" class="nav-link"> Get Funding</a></li>
                <li class="nav-item cta cta-colored"><a href="{{ url('register')  }}" class="nav-link"> Trusted Partners</a></li>{{--<span class="icon-pencil"></span>--}}
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->