@extends('layouts.app')
@section('content')
    <div class="hero-wrap" style="background-image: url({{ url('app-assets/images/bg_1.jpg') }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><span class="mr-2"><a href="#">Home</a></span> <span>Blog Details</span></p>
                    <h1 class="mb-3 bread">Blog Detail</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section ftco-degree-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ftco-animate">
                    <h2 class="mb-3">{{ $single_blog->title }}</h2>
                    <img src="{{asset("public/images/$single_blog->picture")}}" alt="" height="486px" width="730px">
                    <br><br>
                    <p>{!! $single_blog->detail !!}</p>
                </div> <!-- .col-md-8 -->
                <div class="col-lg-4 sidebar ftco-animate">
                    <div class="sidebar-box ftco-animate">
                        <h3>Recent Blog</h3>
                        {{--                        {{request()->route('id')}}--}}
                        @foreach($all_posts as $total_post)
                            @if(request()->route('id') != $total_post->id)
                                <div class="block-21 mb-4 d-flex">
                                    <a href="{{ url('blog/'.$total_post->id) }}" class="blog-img mr-4" style="background-image: url({{asset('/public/images/'.$total_post->picture)}});"></a>
                                    <div class="text">
                                        <h3 class="heading"><a href="{{ url('blog/'.$total_post->id) }}">{{ $total_post->title }}</a></h3>
                                        <div class="meta">
                                            {{--                                    <div><a href="#"><span class="icon-calendar"></span> July 12, 2018</a></div>--}}
                                            <div><a href="{{ route('login') }}"><span class="icon-person"></span> Admin</a></div>
                                            {{--                                    <div><a href="#"><span class="icon-chat"></span> 19</a></div>--}}
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section> <!-- .section -->
@endsection
@section('scripts')

@endsection