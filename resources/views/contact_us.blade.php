@extends('layouts.app')
@section('content')
    <div class="hero-wrap" style="background-image: url({{ url('app-assets/images/bg_1.jpg') }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="#">Home</a></span> <span>Contact Us</span></p>
                    <h1 class="mb-3 bread">Contact Us</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section contact-section bg-light">
        <div class="container">
{{--            <div class="row d-flex mb-5 contact-info">--}}
{{--                <div class="col-md-12 mb-4">--}}
{{--                    <h2 class="h3">Contact Information</h2>--}}
{{--                </div>--}}
{{--                <div class="w-100"></div>--}}
{{--                <div class="col-md-3 d-flex">--}}
{{--                    <div class="info bg-white p-4">--}}
{{--                        <p><span>Address:</span> Austin, TX - no link to address </p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 d-flex">--}}
{{--                    <div class="info bg-white p-4">--}}
{{--                        <p><span>Phone:</span> <a href="tel://1234567920">(512) 580-7002 </a></p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 d-flex">--}}
{{--                    <div class="info bg-white p-4">--}}
{{--                        <p><span>Email:</span> <a href="mailto:info@yoursite.com">info@hornet.capital </a></p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-md-3 d-flex">--}}
{{--                    <div class="info bg-white p-4">--}}
{{--                        <p><span>Website: </span> <a href="http://mou.ranksol.com/Real_Estate/">Hornet Capital</a></p>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
            <div class="row justify-content-center mb-2 pb-3">
                <div class="col-md-7 heading-section text-center ftco-animate">
                    <h2 class="mb-4"> Contact Us</h2>
                </div>
            </div>
            <div class="row block-9">
{{--                <div class="col-md-6 order-md-last d-flex">--}}
{{--                    <form action="#" class="bg-white p-5 contact-form">--}}
{{--                        <div class="form-group">--}}
{{--                            <lable for="">Website:-</lable>--}}
{{--                            <br>--}}
{{--                            <p><a href="{{ url('http://mou.ranksol.com/Real_Estate/') }}">Hornet Capital</a></p>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <lable for="">Phone:-</lable>--}}
{{--                            <br>--}}
{{--                            <p>(512) 580-7002</p>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <lable for="">Email:-</lable>--}}
{{--                            <br>--}}
{{--                            <p>info@hornet.capital</p>--}}
{{--                        </div>--}}
{{--                        <div class="form-group">--}}
{{--                            <lable for="">Address:-</lable>--}}
{{--                            <br>--}}
{{--                            <p>Austin, TX - no link to address</p>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}

                <div class="col-md-6 order-md-last">
                    <div class="row block-9">
                        <div class="col-md-12 d-flex">
                            <div class="info bg-white p-4">
                                <p><span>Website: </span><br> <a href="http://mou.ranksol.com/Real_Estate/">Hornet Capital</a></p>
                            </div>
                        </div>
                        <div class="col-md-12 d-flex">
                            <div class="info bg-white p-4">
                                <p><span>Email:</span><br> info@hornet.capital</p>
                            </div>
                        </div>
                        <div class="col-md-12 d-flex">
                            <div class="info bg-white p-4">
                                <p><span>Phone:</span><br> (512) 580-7002</p>
                            </div>
                        </div>
                        <div class="col-md-12 d-flex">
                            <div class="info bg-white p-4">
                                <p><span>Address:</span><br> Austin, TX </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 order-md-last d-flex">
                    <form action="#" class="bg-white p-5 contact-form">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your Name">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Your Email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Subject">
                        </div>
                        <div class="form-group">
                            <textarea name="" id="" cols="30" rows="7" class="form-control" placeholder="Message"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Send Message" class="btn btn-primary py-3 px-5">
                        </div>
                    </form>

                </div>
{{--                <div class="col-md-6 d-flex">--}}
{{--                    <div id="map" class="bg-white"></div>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>
@endsection