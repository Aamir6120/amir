@extends('layouts.app')
@section('content')
    <section id="home_slider" class="home-slider owl-carousel">
        @foreach($all_listing as $total_listing)
            <div class="slider-item" style="background-image:url({{asset('/public/images/listing/'.$total_listing->picture)}});">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row no-gutters slider-text align-items-md-end align-items-center justify-content-end">
                        <div class="col-md-6 text p-4 ftco-animate"><h1 class="mb-3">{{ $total_listing->title  }}</h1>
                            <span class="location d-block mb-3"><i class="icon-my_location"></i> {{ $total_listing->location  }}</span>
                            <p>{!! $total_listing->detail !!}</p>                        <span
                                    class="price">{{ $total_listing->Price  }}</span>
                        </div>{{--url({{url($total_listing->picture)}});--}}
                    </div>
                </div>
            </div>
        @endforeach
    </section>
    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row d-flex">
                <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services py-4 d-block text-center">
                        <div class="d-flex justify-content-center">
                            <div class="icon"><span class="flaticon-pin"></span></div>
                        </div>
                        <div class="media-body p-2 mt-2"><h3 class="heading mb-3">Education & Resources</h3>
                            <p>Investment based education, thought leaders, continuous education and resources to be
                                successful.</p></div>
                    </div>
                </div>
                <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services py-4 d-block text-center">
                        <div class="d-flex justify-content-center">
                            <div class="icon"><span class="flaticon-detective"></span></div>
                        </div>
                        <div class="media-body p-2 mt-2"><h3 class="heading mb-3">Trusted Network</h3>
                            <p>Part of the Hornet Advantage is access to our trusted real estate agents, contractors,
                                wholesalers, investors, and funding.</p></div>
                    </div>
                </div>
                <div class="col-md-3 d-flex align-sel Searchf-stretch ftco-animate">
                    <div class="media block-6 services py-4 d-block text-center">
                        <div class="d-flex justify-content-center">
                            <div class="icon"><span class="flaticon-house"></span></div>
                        </div>
                        <div class="media-body p-2 mt-2"><h3 class="heading mb-3">Smart Funding</h3>
                            <p>100% financing, Bridge and Gap Money, Construction Funding, Education, and Resources.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 d-flex align-self-stretch ftco-animate">
                    <div class="media block-6 services py-4 d-block text-center">
                        <div class="d-flex justify-content-center">
                            <div class="icon"><span class="flaticon-purse"></span></div>
                        </div>
                        <div class="media-body p-2 mt-2"><h3 class="heading mb-3">Investments</h3>
                            <p>Diversify your portfolio with Hornet Capitals investment opportunity. Join our fund as an
                                accredited investor, or join our projects with our fractional lending. </p></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section ftco-properties">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 heading-section text-center ftco-animate"><span
                            class="subheading">Recent Posts</span>
                    <h2 class="mb-4">Recent Projects</h2></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="properties-slider owl-carousel ftco-animate">
                        @foreach($all_properties as $total_properties)
                            <div class="item">
                                <div class="properties"><a href="#"
                                                           class="img d-flex justify-content-center align-items-center"
                                                           style="background-image:url({{asset('/public/images/property/'.$total_properties->picture)}});">
                                        <div class="icon d-flex justify-content-center align-items-center"><span
                                                    class="icon-search2"></span></div>
                                    </a>
                                    <div class="text p-3">
                                        <div class="d-flex"><span
                                                    class="status rent">{{ $total_properties->status  }}</span>
                                            <div class="one"><h3><a href="#">{{ $total_properties->title  }}</a></h3>
                                                <p>{{ $total_properties->category  }}</p></div>
                                            <div class="two"><span class="price">{{ $total_properties->price  }} <small>/ {{ $total_properties->month  }}</small></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section ftco-counter img" id="section-counter"
             style="background-image:url({{url('app-assets/images/bg_1.jpg')}});">
        <div class="container">
            <div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-7 text-center heading-section heading-section-white ftco-animate"><h2 class="mb-4">
                        Some fun facts</h2></div>
            </div>
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18 text-center">
                                <div class="text"><strong class="number" data-number="9000">0</strong>
                                    <span>Resources</span></div>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18 text-center">
                                <div class="text"><strong class="number" data-number="10000">0</strong>
                                    <span>Projects</span></div>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18 text-center">
                                <div class="text"><strong class="number" data-number="1000">0</strong> <span>Trusted Partners</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 d-flex justify-content-center counter-wrap ftco-animate">
                            <div class="block-18 text-center">
                                <div class="text"><strong class="number" data-number="100">0</strong> <span>Opportunities</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section testimony-section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 ftco-animate">
                    <div class="row ftco-animate">
                        <div class="col-md-12">
                            <div class="carousel-testimony owl-carousel ftco-owl">
                                @foreach($all_reviews as $total_reviews)
                                    <div class="item">
                                        <div class="testimony-wrap py-4 pb-5">
                                            <div class="user-img mb-4"
                                                 style="background-image:url({{asset('/public/images/reviews/'.$total_reviews->picture)}});"><span
                                                        class="quote d-flex align-items-center justify-content-center">		                      <i
                                                            class="icon-quote-left"></i>		                    </span>
                                            </div>
                                            <div class="text text-center"><p
                                                        class="mb-4">{!! $total_reviews->detail !!}</p>
                                                <p class="name">{{ $total_reviews->title  }}</p>
                                                <span class="position">{{ $total_reviews->role  }}</span></div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="ftco-section">
        <div class="container">
            <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-7 heading-section text-center ftco-animate"><span
                            class="subheading">Read Blogs</span>
                    <h2>Recent Blog</h2></div>
            </div>
            <div class="row d-flex">
                @foreach($all_posts as $post)
                    <div class="col-md-3 d-flex ftco-animate">
                        <div class="blog-entry align-self-stretch" style="width: 100%;"><a
                                    href="{{ url('blog/'.$post->id) }}" class="block-20"
                                    style="background-image:url({{asset('/public/images/'.$post->picture)}}); background-size: cover;"> </a>
                            <div class="text mt-3 d-block">
                                <h3>{{ $post->title  }}</h3>
                                <div class="meta mb-3">
                                    <div><a href="{{ route('login') }}">Admin</a>
                                    </div>{{--                                <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <section class="ftco-section-parallax">
        <div class="parallax-img d-flex align-items-center">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-7 text-center heading-section heading-section-white ftco-animate"><h2>Subcribe to
                            our Newsletter</h2>
                        <p>Want The Latest Updates From Hornet Capital?</p>

                        <div class="alert my_alert" role="alert" hidden>
                            <button type="button" class="close" onclick="$(this).parent().attr('hidden', true)">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <p class="text-muted"></p>
                        </div>

                        <div class="row d-flex justify-content-center mt-5">
                            <div class="col-md-8">
                                <div class="subscribe-form">
                                    @csrf
                                    <div class="form-group d-flex">
                                        <input type="email" name="email" id="sub_email" class="form-control"placeholder="Enter email address">
                                        <input type="button" value="Subscribe" class="submit px-3" onclick="addsubscribe()">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
