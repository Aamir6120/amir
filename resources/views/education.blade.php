@extends('layouts.app')
@section('content')
    <div class="hero-wrap" style="background-image: url({{ url('app-assets/images/bg_1.jpg') }});">
        <div class="overlay"></div>
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <p class="breadcrumbs"><span class="mr-2"><a href="#">Home</a></span> <span>Education</span></p>
                    <h1 class="mb-3 bread">Education</h1>
                </div>
            </div>
        </div>
    </div>

    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row d-flex">
                @foreach($all_education as $total_education)
                <div class="col-md-3 d-flex ftco-animate">
                    <div class="blog-entry align-self-stretch" style="width: 100%;">
                        <a href="{{ url('blog/'.$total_education->id) }}" class="block-20" style="background-image: url({{asset('/public/images/'.$total_education->picture)}});">
                        </a>
                        <div class="text mt-3 d-block">
                            <h3 class="heading mt-3"><a href="{{ url('blog/'.$total_education->id) }}">{{ $total_education->title }}</a></h3>
                            <div class="meta mb-3">
                                <div><a href="{{ route('login') }}">Admin</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection
