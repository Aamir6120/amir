<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::group(['middleware' => ['auth']], function () {
    // Start Admin routes

	Route::get('/admin', 'AdminController@dashboard');
	Route::get('users/{id}', 'AdminController@users');
	Route::get('active_user', 'AdminController@active_user')->name('active_user');
	Route::get('pending_user', 'AdminController@pending_user')->name('pending_user');
	Route::get('delete_user', 'AdminController@delete_user')->name('delete_user');
	Route::get('user_detail/{id}', 'AdminController@user_detail');
	Route::get('all_users', 'AdminController@all_users');
	Route::get('loadUsers', 'AdminController@loadUsers')->name('loadUsers');
	Route::get('loadUsers1/{id}', 'AdminController@loadUsers1')->name('loadUsers1');

	Route::get('active', 'AdminController@activer')->name('active');

	Route::get('add_blog_post', 'BlogController@add_blog_post')->name('add_blog_post');
	Route::post('new_post', 'BlogController@new_post')->name('new_post');
	Route::get('all_posts', 'BlogController@all_posts')->name('all_posts');
	Route::get('all_blog_posts', 'BlogController@all_blog_posts')->name('all_blog_posts');
	Route::get('edit_blog_post/{id}', 'BlogController@edit_blog_post')->name('edit_blog_post');
	Route::post('update_post', 'BlogController@update_post')->name('update_post');
	Route::get('delete_blog_post', 'BlogController@delete_blog_post')->name('delete_blog_post');

	// Reviews routes start
	Route::get('reviews', 'ReviewController1@index')->name('reviews');
	Route::get('get_review', 'ReviewController1@get_review')->name('get_review');
	Route::get('add_review', 'ReviewController1@add_review')->name('add_review');
	Route::get('review', 'ReviewController1@all_review')->name('review');
	Route::post('new_review', 'ReviewController1@new_review')->name('new_review');
	Route::get('edit_review/{id}', 'ReviewController1@edit_review')->name('edit_review');
	Route::get('delete_review_post', 'ReviewController1@delete_review_post')->name('delete_review_post');
	Route::post('edit_form_review', 'ReviewController1@edit_form_review')->name('edit_form_review');

	// Properties routes start
	Route::get('properties', 'PropertyController@index')->name('properties');
	Route::get('all_properties', 'PropertyController@all_properties')->name('all_properties');
	Route::get('add_property', 'PropertyController@add_property')->name('add_property');
	Route::post('add_new_property', 'PropertyController@add_new_property')->name('add_new_property');
	Route::get('delete_property_post', 'PropertyController@delete_property_post')->name('delete_property_post');
	Route::get('edit_property/{id}', 'PropertyController@edit_property')->name('edit_property');
	Route::post('edit_property_post', 'PropertyController@edit_property_post')->name('edit_property_post');

	// Listing routes starts
	Route::get('listings', 'ListingController@index')->name('listings');
	Route::get('all_listings', 'ListingController@all_listings')->name('all_listings');
	Route::get('new_listing', 'ListingController@new_listing')->name('new_listing');
	Route::post('add_listing', 'ListingController@add_listing')->name('add_listing');
	Route::get('delete_listing', 'ListingController@delete_listing')->name('delete_listing');
	Route::get('edit_listing/{id}', 'ListingController@edit_listing')->name('edit_listing');
	Route::post('update_listing', 'ListingController@update_listing')->name('update_listing');
	Route::post('transition', 'ListingController@transition')->name('transition');
	Route::get('pause_listing', 'ListingController@pause_listing')->name('pause_listing');
	Route::get('play_listing', 'ListingController@play_listing')->name('play_listing');


	Route::get('send_message', 'MessageController@send_message')->name('send_message');
	Route::post('message_insert', 'MessageController@message_insert')->name('message_insert');

    // Subscriber routes starts
    Route::get('subscriber', 'SubscribeController@subscribers')->name('subscriber');
    Route::post('subscribers_message', 'SubscribeController@subscribers_message')->name('subscribers_message');
//    Route::get('message', 'SubscribeController@subscribemessage')->name('message');

    // End of admin routes

    // Starts Users routes

    Route::get('/dashboard', 'HomeController@UserDashboard')->name('dashboard');
    Route::get('/profile', 'HomeController@UserProfile')->name('profile');
    Route::get('/messages', 'UserMessageController@UserMessages')->name('messages'); /// new
    Route::get('/reply/{id}', 'UserMessageController@UserReply')->name('reply'); /// new
    Route::post('message_reply', 'UserMessageController@message_reply')->name('message_reply'); /// new

    // End of Users Route
});

Auth::routes();
Route::get('/about', 'AboutController@about')->name('about');
Route::get('/education', 'BlogController@education')->name('education');
Route::get('blog/{id}', 'BlogController@blog_view')->name('blog');
Route::get('/privacypolicy', 'PrivacyprotectionController@privacyprotection')->name('privacy');
Route::get('/termsofservice', 'ServicesController@services')->name('services');
Route::get('/contact', 'ContactController@contact')->name('contact');
// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'IndexController@display_blog');

Route::post('add_subscriber', 'SubscribeController@add_subscriber')->name('add_subscriber');