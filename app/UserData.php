<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserData extends Model
{
    //
    protected $fillable = [
        'user_id', 'name', 'business_name', 'broker_name', 'address', 'broker_address', 'licensed', 'licensed_info', 'specialty_type', 'experience', 'website', 'insurance', 'insurance_type', 'employee_before', 'pay_workmans_comp_insurance', 'insurance_proof', 'work_with_investors_before', 'where_you_find_us', 'referral1_name', 'referral1_email', 'referral1_phone', 'referral2_name', 'referral2_email', 'referral2_phone', 'referral3_name', 'referral3_email', 'referral3_phone', 'is_individual', 'meet_accredited_investor_requirement', 'investment', 'started_date', 'money_borrow', 'project_type', 'loan_type', 'project_or_loan_address',
    ];
}
