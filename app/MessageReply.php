<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageReply extends Model
{
    //
    protected $fillable = [
        'message_id', 'reply',
    ];
}
