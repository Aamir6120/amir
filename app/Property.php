<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    //
    protected $fillable = [
        'title', 'status', 'category', 'price', 'month', 'picture',
    ];
}
