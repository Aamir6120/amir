<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Blog;
use App\Subscribe;
use Mail;
use yajra\Datatables\Datatables;


class BlogController extends Controller
{
    //
    public function education(){
        $all_education = Blog::all();
        return view('education', compact('all_education', $all_education));
    }

    public function blog_view(Request $request){
        $single_blog = Blog::where('id', $request->id)->first();
        $all_posts = Blog::all()->take(4);
        return view('blog', compact('single_blog', $single_blog,'all_posts', $all_posts));
    }

    public function add_blog_post(){
        return view('admin.blog.add_new_blog');
    }

    public function new_post(Request $request){
        $credentials = $request->only('title', 'detail', 'picture');
        $rules = [
            'title' => 'required',
            'detail' => 'required',
            'picture' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return back()->with('error', $validator->messages());
        }

        $picture = uniqid() . '.' . $request->picture->getClientOriginalExtension("imageQuestion");
        $request->picture->move(public_path("images"), $picture);

        $imagepath = url("/public/images/" . $picture);

        $add_post = Blog::create([
            'title' => $request->title,
            'detail' => $request->detail,
            'picture' => $picture,
        ]);
        if($add_post){

            $sub = Subscribe::all()->count();

            
            if($sub != 0){
                $email = "info@hornet.com";

                $subject = "New blog post addede by admin";

                if($sub == 1){
                    Mail::send('email.subscribers', ['email' => $email, 'post_id' => $add_post->id],
                        function ($mail) use ($email, $subject) {
                            $firstsubscriber = Subscribe::all();
                            $mail->from(getenv('MAIL_FROM_ADDRESS'), "info@hornet.com");
                            $mail->to($firstsubscriber[0]->email);
                            $mail->subject($subject);
                        });
                }
                else{
                    Mail::send('email.subscribers', ['email' => $email, 'post_id' => $add_post->id],
                        function ($mail) use ($email, $subject) {
                            $firstsubscriber = Subscribe::all()->take(1);
                            $subscribers = Subscribe::all();

                            $mail->from(getenv('MAIL_FROM_ADDRESS'), "info@hornet.com");
                            $mail->to($firstsubscriber[0]->email);
                            $count = 0;
                            foreach($subscribers as $subscribe){

                                if($count != 0){

                                    $mail->bcc($subscribe->email);
                                }
                                $count++;
                            }
                            $mail->subject($subject);
                        });
                }
            }

            $posts = Blog::all();
            return view('admin.blog.all_posts', compact('posts'))->with('message', 'Post created successfully');
        }
    }

    public function all_posts(){
        $posts = Blog::all();
        if($posts){
            return view('admin.blog.all_posts', compact($posts));
        }
    }

    public function all_blog_posts(){
        
        $posts = Blog::all();

        $count = 0;
        return Datatables::of($posts)
            ->addColumn('count', function($posts){global $count;
                $count++;
                return $count;
            })
            ->addColumn('image', function ($posts)
            {
                $c =   '<img src="' . url("/public/images/$posts->picture") .'" style = "width:100px; height = 100px;">';
                return $c;
            })
            ->addColumn('description', function ($posts)
            {
                $ae =str_limit($posts->detail, 100);
                return $ae;
            })

             ->addColumn('action', function ($posts)
             {
                 $ac ='<a onclick = "delete_blog_post('.$posts->id.')" style="margin-right: 5px; cursor:pointer;" title="Delete"><i class="fa fa-fw fa-trash text-danger"></i></a>
                     <a href="'.url('edit_blog_post/'.$posts->id).'" title="Edit"><i class="fa fa-edit text-primary"></i></a>';
                 return $ac;
             })
            ->rawColumns([
                'count',
                'image',
                'action',
                'description',
            ])
            
            ->make(true);
    }

    public function edit_blog_post($id){
        $post = Blog::where('id', $id)->first();
        if($post){
            return view('admin.blog.edit_post', compact('post'));
        }
    }

    public function update_post(Request $request){
        $credentials = $request->only('id', 'title', 'detail', 'picture');
        $rules = [
            'id' => 'required',
            'title' => 'required',
            'detail' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return back()->with('error', $validator->messages());
        }

        if(!empty($request->picture)){

            $picture = uniqid() . '.' . $request->picture->getClientOriginalExtension("imageQuestion");
            $request->picture->move(public_path("images"), $picture);

            $imagepath = url("/public/images/" . $picture);

            $update_post = Blog::where('id', $request->id)->update([
                'title' => $request->title,
                'detail' => $request->detail,
                'picture' => $picture,
            ]);
        }
        else{
           $update_post = Blog::where('id', $request->id)->update([
                'title' => $request->title,
                'detail' => $request->detail,
            ]); 
        }
            if($update_post){
                $posts = Blog::all();
                return view('admin.blog.all_posts', compact('posts'))->with('message', 'Post updated successfully');
            }
    }

    public function delete_blog_post(Request $request){
        $post_delete = Blog::where('id', $request->id)->delete();
        if($post_delete){
            return 1;
        }
        else{
            return 0;
        }
    }
    
}
