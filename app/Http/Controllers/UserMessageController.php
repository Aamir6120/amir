<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;
use App\User;
use App\MessageReply;
use Validator;
use Mail;
use Illuminate\Support\Facades\Auth;

class UserMessageController extends Controller
{
    public function UserMessages()
    {
    	$id = Auth::id();
    	$user_role = User::where('id', $id)->value('role');

    	$messages = Message::where('role_id', $user_role)->orderBy('id', 'desc')->paginate(10);

        return view('users.message.messages', compact('messages'));
    }

    public function UserReply($id)
    {
    	$message = Message::where('id', $id)->first();
        return view('users.message.reply', compact('message'));
    }

    public function message_reply(Request $request){
    	$credentials = $request->only('message_id', 'detail');
		$rules = [

			'message_id' => 'required',
			'detail' => 'required',
		];
		$validator = Validator::make($credentials, $rules);
		if ($validator->fails()) {
			return back()->with('error', $validator->messages());
		}

		$reply = $request->detail;
		$message_id = $request->message_id;

		$create = MessageReply::create([
			'message_id' => $message_id,
			'reply' => $reply,
		]);	
		if($create){

			$role = Auth::user()->role;

			if($role == 2){
				$name = "Trusted Contractors";
			}

			if($role == 3){
				$name = "Trusted Realtor";
			}

			if($role == 4){
				$name = "Trusted Wholesaler";
			}
			if($role == 5){
				$name = "Trusted Accredited Investor";
			}
			if($role == 6){
				$name = "Trusted Investor";
			}
			if($role == 7){
				$name = "Smart Funding";
			}
			
			$email = Auth::user()->email;
			$subject = "A " .  $name . " user replied your message";
	        Mail::send('email.user_reply', ['email' => $email, 'name' => $name, 'detail' => $request->detail],
	            function ($mail) use ($email, $name, $subject) {
	                $mail->from(getenv('MAIL_FROM_ADDRESS'), "info@hornet.com");
	                $mail->to("aamirjutt6120@gmail.com");
	                $mail->subject($subject);
	            });


			return back()->with('status', 'You have successfully replied to admin');
		}

    }
}
