<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use yajra\Datatables\Datatables;
use App\Review;

class ReviewController1 extends Controller
{
    //
    public function index(){
    	$reviews = Review::all();
    	return view('admin.reviews.all_reviews', compact('reviews'));
    }
    
    public function all_review(){
    	$reviews = Review::all();
    	return view('admin.reviews.all_reviews', compact('reviews'));
    }

    public function add_review(Request $request){
    	return view('admin.reviews.add_review');
    }

    public function new_review(Request $request){
    	$credentials = $request->only('title', 'role', 'detail', 'picture');
		$rules = [
			'title' => 'required',
			'role' => 'required',
			'detail' => 'required',
			'picture' => 'required',
		];
		$validator = Validator::make($credentials, $rules);
		if ($validator->fails()) {
			return back()->with('error', $validator->messages());
		}

		$picture = uniqid() . '.' . $request->picture->getClientOriginalExtension("imageQuestion");
		$request->picture->move(public_path("images/reviews"), $picture);

		$imagepath = url("/public/images/reviews/" . $picture);

		$add_review = Review::create([
			'title' => $request->title,
			'role' => $request->role,
			'detail' => $request->detail,
			'picture' => $picture,
		]);
		if($add_review){
			$reviews = Review::all();
			return view('admin.reviews.all_reviews', compact('reviews'))->with('message', 'Review created successfully');
		}
    }

    public function get_review(){
        $posts = Review::all();

        $count = 0;
        return Datatables::of($posts)
            ->addColumn('count', function($posts){global $count;
                $count++;
                return $count;
            })
            ->addColumn('image', function ($posts)
            {
                $c =   '<img src="' . url("/public/images/reviews/$posts->picture") .'" style = "width:100px; height = 100px;">';
                return $c;
            })

            ->addColumn('description', function ($posts)
            {
                $ae =str_limit($posts->detail, 100);
                return $ae;
            })
            

             ->addColumn('action', function ($posts)
             {
                 $ac ='<a onclick = "delete_review_post('.$posts->id.')" style="margin-right: 5px; cursor:pointer;" title="Delete"><i class="fa fa-fw fa-trash text-danger"></i></a>
                     <a href="'.url('edit_review/'.$posts->id).'" title="Edit"><i class="fa fa-edit text-primary"></i></a>';                                  
                 return $ac;
             })
            ->rawColumns([
                'count',
                'image',
                'description',
                'action',
            ])
            
            ->make(true);
    }

    public function delete_review_post(Request $request){
    	$post_delete = Review::where('id', $request->id)->delete();
        if($post_delete){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function edit_review($id){
    	$review = Review::where('id', $id)->first();
    	return view('admin.reviews.edit_review', compact('review'));
    }

    public function edit_form_review(Request $request){
    	$credentials = $request->only('id', 'title', 'role', 'detail', 'picture');
		$rules = [
			'id' => 'required',
			'title' => 'required',
			'role' => 'required',
			'detail' => 'required',
		];
		$validator = Validator::make($credentials, $rules);
		if ($validator->fails()) {
			return back()->with('error', $validator->messages());
		}

        if(!empty($request->picture)){

    		$picture = uniqid() . '.' . $request->picture->getClientOriginalExtension("imageQuestion");
    		$request->picture->move(public_path("images/reviews"), $picture);

    		$imagepath = url("/public/images/reviews/" . $picture);

    		$add_review = Review::where('id', $request->id)->update([
    			'title' => $request->title,
    			'role' => $request->role,
    			'detail' => $request->detail,
    			'picture' => $picture,
    		]);
        }
        else{
            $add_review = Review::where('id', $request->id)->update([
                'title' => $request->title,
                'role' => $request->role,
                'detail' => $request->detail,
            ]);
        }
		if($add_review){
			$reviews = Review::all();
			return view('admin.reviews.all_reviews', compact('reviews'))->with('message', 'Review updated successfully');
		}
    }
}
