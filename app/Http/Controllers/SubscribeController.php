<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Mail;
use App\SubscriberMessage;
use App\Subscribe;

class SubscribeController extends Controller
{
    public function subscribers()
    {

        $subscribers = Subscribe::orderBy('id', 'Desc')->paginate(10);
        return view('admin.subscriber.subscribers', compact('subscribers'));
    }
    public function subscribemessage()
    {
        return view('admin.subscriber.messages');
    }

    public function add_subscriber(Request $request){
    	$credentials = $request->only('email');
		$rules = [

			'email' => 'required|string|email|max:255|unique:subscribes',
		];
		$validator = Validator::make($credentials, $rules);
		if ($validator->fails()) {
		    $already = "You have already subscribed";
			return  response()->json([ 'success' => false, 'message' => $already ]);
		}

		$create = Subscribe::create([
			'email' =>$request->email,
		]);

		if($create){
            $success = "You have succesfully subscribed";
            return  response()->json([ 'success' => true, 'message' => $success ]);
        }
    }

    public function subscribers_message(Request $request){
        $message = $request->message;
        $userArray = $request->userArray;

        $count_email = count($userArray);
        $email = "info@hornet.com";
        $name = "Hornet";

         $subject = "New message from Hornet Admin";

        if($count_email == 1){
            Mail::send('email.subscribers_message', ['detail' => $request->message],
                function ($mail) use ($email, $name, $userArray, $subject) {
                    $mail->from(getenv('MAIL_FROM_ADDRESS'), "info@hornet.com");
                    $mail->to($userArray[0]);
                    $mail->subject($subject);
                    echo $userArray[0];
                });
        }
        else{
            Mail::send('email.subscribers_message', ['detail' => $request->message],
                function ($mail) use ($email, $name, $userArray, $subject) {

                   $mail->from(getenv('MAIL_FROM_ADDRESS'), "info@hornet.com");
                    $mail->to($userArray[0]);
                    $count = 0;
                    foreach($userArray as $email){

                        if($count != 0){

                            $mail->bcc($email);
                        }
                        $count++;
                    }
                    $mail->subject($subject);
                });
        }
        
        foreach($userArray as $email){
            $create = SubscriberMessage::create([
                'email' => $email,
                'message' => $message,
            ]);
        }
    }
}
