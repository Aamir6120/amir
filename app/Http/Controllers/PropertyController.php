<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Property;
use Validator;
use yajra\Datatables\Datatables;

class PropertyController extends Controller
{
    //
    public function index(){
    	$proprties = Property::all();
    	return view('admin.properties.property', compact('proprties'));
    }

    public function all_properties(){
    	$posts = Property::all();

        $count = 0;
        return Datatables::of($posts)
            ->addColumn('count', function($posts){global $count;
                $count++;
                return $count;
            })
            ->addColumn('image', function ($posts)
            {
                $c =   '<img src="' . url("/public/images/property/$posts->picture") .'" style = "width:100px; height = 100px;">';
                return $c;
            })
             ->addColumn('description', function ($posts)
            {
                $ae =str_limit($posts->detail, 100);
                return $ae;
            })
            

             ->addColumn('action', function ($posts)
             {
                 $ac ='<a onclick = "delete_property('.$posts->id.')" style="margin-right: 5px; cursor:pointer;" title="Delete"><i class="fa fa-fw fa-trash text-danger"></i></a>
                     <a href="'.url('edit_property/'.$posts->id).'" title="Edit"><i class="fa fa-edit text-primary"></i></a>';                                  
                 return $ac;
             })
            ->rawColumns([
                'count',
                'image',
                'description',
                'action',
            ])
            
            ->make(true);
    }

    public function add_property(){
    	return view('admin.properties.add_property');
    }

    public function add_new_property(Request $request){
    	$credentials = $request->only('title', 'status', 'category', 'price', 'month', 'picture');
		$rules = [
			'title' => 'required',
			'status' => 'required',
			'category' => 'required',
			'price' => 'required',
			'month' => 'required',
			'picture' => 'required',

		];
		$validator = Validator::make($credentials, $rules);
		if ($validator->fails()) {
			return back()->with('error', $validator->messages());
		}

		$picture = uniqid() . '.' . $request->picture->getClientOriginalExtension("imageQuestion");
		$request->picture->move(public_path("images/property"), $picture);

		$imagepath = url("/public/images/property/" . $picture);

		$add_property = Property::create([
			'title' => $request->title,
			'status' => $request->status,
			'category' => $request->category,
			'price' => $request->price,
			'month' => $request->month,
			'picture' => $picture,
		]);
		if($add_property){
			$properties = Property::all();
			return view('admin.properties.property', compact('properties'))->with('message', 'Property created successfully');
		}
    }

    public function delete_property_post(Request $request){
        $post_delete = Property::where('id', $request->id)->delete();
        if($post_delete){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function edit_property($id){
        $property = Property::where('id', $id)->first();
        if($property){
            return view('admin.properties.edit_property', compact('property'));
        }
    }

    public function edit_property_post(Request $request){
        $credentials = $request->only('id', 'title', 'status', 'category', 'price', 'month', 'picture');
        $rules = [
            'id' => 'required',
            'title' => 'required',
            'status' => 'required',
            'category' => 'required',
            'price' => 'required',
            'month' => 'required',

        ];
        $validator = Validator::make($credentials, $rules);
        if ($validator->fails()) {
            return back()->with('error', $validator->messages());
        }

        if(!empty($request->picture)){

            $picture = uniqid() . '.' . $request->picture->getClientOriginalExtension("imageQuestion");
            $request->picture->move(public_path("images/property"), $picture);

            $imagepath = url("/public/images/property/" . $picture);

            $add_property = Property::where('id', $request->id)->update([
                'title' => $request->title,
                'status' => $request->status,
                'category' => $request->category,
                'price' => $request->price,
                'month' => $request->month,
                'picture' => $picture,
            ]);
        }
        else{
            $add_property = Property::where('id', $request->id)->update([
                'title' => $request->title,
                'status' => $request->status,
                'category' => $request->category,
                'price' => $request->price,
                'month' => $request->month,
            ]);
        }
        if($add_property){
            $properties = Property::all();
            return view('admin.properties.property', compact('properties'))->with('message', 'Property updated successfully');
        }
    }
}
