<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Auth;
use App\UserData;
use App\Transition;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Auth\Events\Registered;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        if($data['role'] == 2){
            return Validator::make($data, [
                'business_name' => ['required', 'string', 'max:255'],
                'address' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'licensed' => ['required'],
                'licensed_info' => ['required', 'string', 'max:255'],
                'specialty_type' => ['required'],
                'experience' => ['required'],
                'website' => ['required'],
                'insurance' => ['required'],
                'insurance_type' => ['required'],
                'employee_before' => ['required'],
                'pay_workmans_comp_insurance' => ['required'],
                'insurance_proof' => ['required'],
                'work_with_investors_before' => ['required'],
                'referral1_name' => ['required', 'string', 'max:255'],
                'referral1_email' => ['required'],
                'referral1_phone' => ['required'],
                'referral2_name' => ['required', 'string', 'max:255'],
                'referral2_email' => ['required'],
                'referral2_phone' => ['required'],
                'referral3_name' => ['required', 'string', 'max:255'],
                'referral3_email' => ['required'],
                'referral3_phone' => ['required'],
                'where_you_find_us' => ['required', 'string', 'max:255'],

            ]);
            dd($data);
        }
        if($data['role'] == 3){
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'broker_name' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'broker_address' => ['required'],
                'licensed' => ['required'],
                'licensed_info' => ['required'],
                'specialty_type' => ['required'],
                'experience' => ['required'],
                'website' => ['required'],
                'work_with_investors_before' => ['required'],
                'referral1_name' => ['required', 'string', 'max:255'],
                'referral1_email' => ['required'],
                'referral1_phone' => ['required'],
                'referral2_name' => ['required', 'string', 'max:255'],
                'referral2_email' => ['required'],
                'referral2_phone' => ['required'],
                'referral3_name' => ['required', 'string', 'max:255'],
                'referral3_email' => ['required'],
                'referral3_phone' => ['required'],
                'where_you_find_us' => ['required', 'string', 'max:255'],
            ]);
        }
        if($data['role'] == 4){
            return Validator::make($data, [
                'business_name' => ['required', 'string', 'max:255'],
                'address' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'licensed' => ['required'],
                'licensed_info' => ['required', 'string', 'max:255'],
                'specialty_type' => ['required'],
                'experience' => ['required'],
                'website' => ['required'],
                'work_with_investors_before' => ['required'],
                'referral1_name' => ['required', 'string', 'max:255'],
                'referral1_email' => ['required'],
                'referral1_phone' => ['required'],
                'referral2_name' => ['required', 'string', 'max:255'],
                'referral2_email' => ['required'],
                'referral2_phone' => ['required'],
                'referral3_name' => ['required', 'string', 'max:255'],
                'referral3_email' => ['required'],
                'referral3_phone' => ['required'],
                'where_you_find_us' => ['required', 'string', 'max:255'],
            ]);
        }
        if($data['role'] == 5){
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'address' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'is_individual' => ['required'],
                'meet_accredited_investor_requirement' => ['required'],
                'investment' => ['required'],
                'started_date' => ['required'],
                'where_you_find_us' => ['required', 'string', 'max:255'],
            ]);
        }
        if($data['role'] == 6){
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'address' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'is_individual' => ['required'],
                'where_you_find_us' => ['required', 'string', 'max:255'],
            ]);
        }
        if($data['role'] == 7){
            return Validator::make($data, [
                'name' => ['required', 'string', 'max:255'],
                'address' => ['required', 'string', 'max:255'],
                'phone' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:6', 'confirmed'],
                'money_borrow' => ['required'],
                'project_type' => ['required'],
                'project_or_loan_address' => ['required', 'string', 'max:255'],
            ]);
        }
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = User::create([

            'phone' => $data['phone'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'role' => $data['role'],
        ]);

        if($data['role'] == 2){
            $type_of_user = "Trusted Contractors";
        }

        if($data['role'] == 3){
            $type_of_user = "Trusted Realtor";
        }
        if($data['role'] == 4){
            $type_of_user = "Trusted Wholesaler";
        }
        if($data['role'] == 5){
            $type_of_user = "Trusted Accredited Investor";
        }
        if($data['role'] == 6){
            $type_of_user = "Trusted Investor";
        }
        if($data['role'] == 7){
            $type_of_user = "Smart Funding";
        }

        $email = $data['email'];
        $name = "Hornet";

        $subject = "New " . $type_of_user . " register";
        Mail::send('email.admin', ['email' => $email, 'type_of_user' => $type_of_user],
            function ($mail) use ($email, $name, $subject) {
                $mail->from(getenv('MAIL_FROM_ADDRESS'), "info@hornet.com");
                $mail->to("info@hornet.capital");
                $mail->subject($subject);
            });


        if($user){
            if($data['role'] == 2){

                $pic = $data['insurance_proof'];
                $picture = uniqid() . '.' . $pic->getClientOriginalExtension("imageQuestion");
                $pic->move(public_path("images"), $picture);
                $imagepath = url("/public/images/" . $picture);

                $userdata = UserData::create([

                    'user_id' => $user->id,
                    'business_name' => $data['business_name'],
                    'address' =>  $data['address'],
                    'licensed' => $data['licensed'],
                    'licensed_info' => $data['licensed_info'],
                    'specialty_type' => $data['specialty_type'],
                    'experience' => $data['experience'],
                    'website' => $data['website'],
                    'insurance' => $data['insurance'],
                    'insurance_type' => $data['insurance_type'],
                    'employee_before' => $data['employee_before'],
                    'pay_workmans_comp_insurance' => $data['pay_workmans_comp_insurance'],
                    'insurance_proof' => $imagepath,
                    'work_with_investors_before' => $data['work_with_investors_before'],
                    'referral1_name' => $data['referral1_name'],
                    'referral1_email' => $data['referral1_email'],
                    'referral1_phone' => $data['referral1_phone'],
                    'referral2_name' => $data['referral2_name'],
                    'referral2_email' => $data['referral2_email'],
                    'referral2_phone' => $data['referral2_phone'],
                    'referral3_name' => $data['referral3_name'],
                    'referral3_email' => $data['referral3_email'],
                    'referral3_phone' => $data['referral3_phone'],
                    'where_you_find_us' => $data['where_you_find_us'],
                ]);
                if($userdata){
                    return $user;
                }
            }


            if($data['role'] == 3){

                $userdata = UserData::create([
                    'user_id' => $user->id,
                    'name' => $data['name'],
                    'broker_name' => $data['broker_name'],
                    'broker_address' => $data['broker_address'],
                    'licensed' => $data['licensed'],
                    'licensed_info' => $data['licensed_info'],
                    'specialty_type' => $data['specialty_type'],
                    'experience' => $data['experience'],
                    'website' => $data['website'],
                    'work_with_investors_before' => $data['work_with_investors_before'],
                    'referral1_name' => $data['referral1_name'],
                    'referral1_email' => $data['referral1_email'],
                    'referral1_phone' => $data['referral1_phone'],
                    'referral2_name' => $data['referral2_name'],
                    'referral2_email' => $data['referral2_email'],
                    'referral2_phone' => $data['referral2_phone'],
                    'referral3_name' => $data['referral3_name'],
                    'referral3_email' => $data['referral3_email'],
                    'referral3_phone' => $data['referral3_phone'],
                    'where_you_find_us' => $data['where_you_find_us'],
                ]);
                if($userdata){
                    return $user;
                }
            }

            if($data['role'] == 4){

                $userdata = UserData::create([
                    'user_id' => $user->id,
                    'business_name' => $data['business_name'],
                    'address' => $data['address'],
                    'licensed' => $data['licensed'],
                    'licensed_info' => $data['licensed_info'],
                    'specialty_type' => $data['specialty_type'],
                    'experience' => $data['experience'],
                    'website' => $data['website'],
                    'work_with_investors_before' => $data['work_with_investors_before'],
                    'referral1_name' => $data['referral1_name'],
                    'referral1_email' => $data['referral1_email'],
                    'referral1_phone' => $data['referral1_phone'],
                    'referral2_name' => $data['referral2_name'],
                    'referral2_email' => $data['referral2_email'],
                    'referral2_phone' => $data['referral2_phone'],
                    'referral3_name' => $data['referral3_name'],
                    'referral3_email' => $data['referral3_email'],
                    'referral3_phone' => $data['referral3_phone'],
                    'where_you_find_us' => $data['where_you_find_us'],
                ]);
                if($userdata){
                    return $user;
                }
            }

            if($data['role'] == 5){

                $userdata = UserData::create([
                    'user_id' => $user->id,
                    'name' => $data['name'],
                    'address' => $data['address'],
                    'is_individual' => $data['is_individual'],
                    'meet_accredited_investor_requirement' => $data['meet_accredited_investor_requirement'],
                    'investment' => $data['investment'],
                    'started_date' => $data['started_date'],
                    'where_you_find_us' => $data['where_you_find_us'],
                ]);
                if($userdata){


                    return $user;
                }
            }

            if($data['role'] == 6){

                $userdata = UserData::create([
                    'user_id' => $user->id,
                    'name' => $data['name'],
                    'address' => $data['address'],
                    'is_individual' => $data['is_individual'],
                    'where_you_find_us' => $data['where_you_find_us'],
                ]);
                if($userdata){
                    return $user;
                }
            }

            if($data['role'] == 7){

                $userdata = UserData::create([
                    'user_id' => $user->id,
                    'name' => $data['name'],
                    'address' => $data['address'],
                    'money_borrow' => $data['money_borrow'],
                    'project_type' => $data['project_type'],
                    'project_or_loan_address' => $data['project_or_loan_address'],
                ]);

                if($userdata){

                    return $user;
                }
            }
        }
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        Auth::logout();
        $all_transition = Transition::all(); 

        return view('auth.login')->with('message', 'User created susscessfully and is under review by admin')->with('all_transition', $all_transition);
    }

    
}
