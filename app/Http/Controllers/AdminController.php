<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\UserData;
use yajra\Datatables\Datatables;

class AdminController extends Controller
{
    //
    public function dashboard(){
    	$contractors = User::where('role', 2)->count();
    	$realtors = User::where('role', 3)->count();
    	$wholeselors = User::where('role', 4)->count();
    	$accredited_investors = User::where('role', 5)->count();
    	$investors = User::where('role', 6)->count();
    	$fundings = User::where('role', 7)->count();

    	return view('admin.dashboard')->with('contractors', $contractors)->with('realtors', $realtors)->with('wholeselors', $wholeselors)->with('accredited_investors', $accredited_investors)->with('investors', $investors)->with('fundings', $fundings);
    }

    public function users($id){
        
    	if($id == 2){
    		$heading= "Contractors";
    		$users = User::where('role', 2)->get();
            $user_id = 2;
    	}
    	else if($id == 3){
    		$heading= "Realtors";
    		$users = User::where('role', 3)->get();
            $user_id = 3;
    	}
    	else if($id == 4){
    		$heading= "Wholeselors";
    		$users = User::where('role', 4)->get();
            $user_id = 4;
    	}
    	else if($id == 5){
    		$heading= "Accredited Investors";
    		$users = User::where('role', 5)->get();
            $user_id = 5;
    	}
    	else if($id == 6){
    		$heading= "Investors";
    		$users = User::where('role', 6)->get();
            $user_id = 6;
    	}
    	else if($id == 7){
    		$heading= "Fundings";
    		$users = User::where('role', 7)->get();
            $user_id = 7;
    	}


    	return view('admin.all_users')->with('user_id', $user_id)->with('heading', $heading)->with('users', $users);

    }

    public function active_user(Request $request){
    	$user_activate = User::where('id', $request->id)->update([
    		'is_verified' => 1,
    	]);
    	if($user_activate){
    		return $request->id;
    	}
    }

    public function pending_user(Request $request){
    	$user_activate = User::where('id', $request->id)->update([
    		'is_verified' => 0,
    	]);
    	if($user_activate){
    		return $request->id;
    	}
    }

    public function delete_user(Request $request){
        $user_delete = User::where('id', $request->id)->delete();
        if($user_delete){
            return 1;
        }
        else{
            return 0;
        }
        
    }

    public function all_users(){
        $users = User::where('role', '!=', 1)->get();
        if($users){
            return view('admin.users', compact('users'));
        }
    }


     public function user_detail($id){
        $user = User::where('id', $id)->first();
        $user_data = UserData::where('user_id', $id)->first();
        if($user){
            return view('admin.user_detail')->with('user', $user)->with('user_data', $user_data);
        }
    }

     public function loadUsers(Request $request)
    {
        $users = User::where('role', '!=', 1);

        $count = 0;
        return Datatables::of($users)


            ->addColumn('count', function($users){global $count;
                $count++;
                return $count;
            })


            ->addColumn('Role', function ($users)
            {
                 if($users->role == 2){
                     $b =   '<span class="label label-success">Contractors</span>';
                 }
                    elseif($users->role == 3){
                       $b = '<span class="label label-primary">Realtor</span>';
                    }
                    elseif($users->role == 4){
                        $b = '<span class="label label-default" style="background-color: #777 !important;">Wholesaler</span>';
                    }
                    elseif($users->role == 5){
                       $b = '<span class="label label-info" style="background-color: #5bc0de !important;">Accredited Investor</span>';
                    }
                    elseif($users->role == 6){
                        $b = '<span class="label label-info" style="background-color: #f0ad4e !important;">Investor</span>';
                    }
                    else if($users->role == 7){
                        $b ='<span class="label label-danger">Smart Funding</span>';
                    }
                                                    
                return $b;
            })
            ->addColumn('status', function ($users)
            {
                if($users->is_verified == 0){
                     $c =   '<span class="label label-primary">Pending</span>';
                 }
                if($users->is_verified == 1){
                   $c = '<span class="label label-success">Active</span>';
                }                                  
                return $c;
            })
            

            ->addColumn('action', function ($users)
            {
                if($users->is_verified == 0){
                     $ac =   '<a onclick = "active_user('.$users->id.')" style="margin-right: 5px; cursor:pointer;" title="Approved"><i class="fa fa-fw fa-check text-success"></i></a>';
                 }
                if($users->is_verified == 1){
                   $ac = '<a onclick = "pending_user('.$users->id.')" style="margin-right: 5px; cursor:pointer;" title="Blocked"><i class="fa fa-fw fa-ban text-warning"></i></a>';
                }
                $ac.='<a onclick = "delete_user('.$users->id.')" style="margin-right: 5px; cursor:pointer;" title="Delete"><i class="fa fa-fw fa-trash text-danger"></i></a>
                    <a href="'.url('user_detail/'.$users->id).'" title="Detail"><i class="fa fa-list text-primary"></i></a>';                                  
                return $ac;
            })

            
            ->rawColumns([
                'count',
                'Role',
                'status',
                'action'
            ])
            
            ->make(true);
    }


    public function loadUsers1($id)
    {
        $users = User::where('role', $id);

        $count = 0;
        return Datatables::of($users)


            ->addColumn('count', function($users){global $count;
                $count++;
                return $count;
            })


            ->addColumn('Role', function ($users)
            {
                 if($users->role == 2){
                     $b =   '<span class="label label-success">Contractors</span>';
                 }
                    elseif($users->role == 3){
                       $b = '<span class="label label-primary">Realtor</span>';
                    }
                    elseif($users->role == 4){
                        $b = '<span class="label label-default" style="background-color: #777 !important;">Wholesaler</span>';
                    }
                    elseif($users->role == 5){
                       $b = '<span class="label label-info" style="background-color: #5bc0de !important;">Accredited Investor</span>';
                    }
                    elseif($users->role == 6){
                        $b = '<span class="label label-info" style="background-color: #f0ad4e !important;">Investor</span>';
                    }
                    else if($users->role == 7){
                        $b ='<span class="label label-danger">Smart Funding</span>';
                    }
                                                    
                return $b;
            })
            ->addColumn('status', function ($users)
            {
                if($users->is_verified == 0){
                     $c =   '<span class="label label-primary">Pending</span>';
                 }
                if($users->is_verified == 1){
                   $c = '<span class="label label-success">Active</span>';
                }                                  
                return $c;
            })
            

            ->addColumn('action', function ($users)
            {
                if($users->is_verified == 0){
                     $ac =   '<a onclick = "active_user('.$users->id.')" style="margin-right: 5px; cursor:pointer;" title="Approved"><i class="fa fa-fw fa-check text-success"></i></a>';
                 }
                if($users->is_verified == 1){
                   $ac = '<a onclick = "pending_user('.$users->id.')" style="margin-right: 5px; cursor:pointer;" title="Blocked"><i class="fa fa-fw fa-ban text-warning"></i></a>';
                }
                $ac.='<a onclick = "delete_user('.$users->id.')" style="margin-right: 5px; cursor:pointer;" title="Delete"><i class="fa fa-fw fa-trash text-danger"></i></a>
                    <a href="'.url('user_detail/'.$users->id).'" title="Detail"><i class="fa fa-list text-primary"></i></a>';                                  
                return $ac;
            })

            
            ->rawColumns([
                'count',
                'Role',
                'status',
                'action'
            ])
            
            ->make(true);
    }

    
}
