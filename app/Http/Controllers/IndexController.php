<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\Review;
use App\Property;
use App\Listing;
use App\Transition;

class IndexController extends Controller
{
    public function display_blog()
    {
        $all_posts = Blog::all()->take(4);
        $all_reviews = Review::all()->take(5);
        $all_properties = Property::all()->take(4);
        $all_listing = Listing::where('is_block', 0)->get()->take(4);
        $all_transition = Transition::all();
        if($all_posts){
            return view('index', compact(
                'all_posts', $all_posts,
                'all_reviews', $all_reviews,
                'all_properties', $all_properties,
                'all_listing', $all_listing,
                'all_transition', $all_transition));
        }
    }
}
