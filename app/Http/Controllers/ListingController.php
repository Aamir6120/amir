<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Listing;
use App\Transition;
use yajra\Datatables\Datatables;


class ListingController extends Controller
{
    //
    public function index(){
    	$listings = Listing::all();
        $transition = Transition::all();
    	return view('admin.listing.listings', compact('listings'), compact('transition'));
    }

    public function all_listings(){
    	$posts = Listing::all();

        $count = 0;
        return Datatables::of($posts)
            ->addColumn('count', function($posts){global $count;
                $count++;
                return $count;
            })
            ->addColumn('image', function ($posts)
            {
                $c =   '<img src="' . url("/public/images/listing/$posts->picture") .'" style = "width:100px; height = 100px;">';
                return $c;
            })
            ->addColumn('description', function ($posts)
            {
                $ae =str_limit($posts->detail, 100);
                return $ae;
            })

             ->addColumn('action', function ($posts)
             {
                if($posts->is_block == 0){
                    $ac = '<a onclick = "pause_listing('.$posts->id.')" style="margin-right: 5px; cursor:pointer;" title="Pause"><i class="fa fa-fw fa-check text-success"></i></a>';
                 
                }
                else{
                     $ac = '<a onclick = "play_listing('.$posts->id.')" style="margin-right: 5px; cursor:pointer;" title="Play"><i class="fa fa-fw fa-ban text-warning"></i></a>';
                }
                 $ac.='<a onclick = "delete_listing('.$posts->id.')" style="margin-right: 5px; cursor:pointer;" title="Delete"><i class="fa fa-fw fa-trash text-danger"></i></a>
                     <a href="'.url('edit_listing/'.$posts->id).'" title="Edit"><i class="fa fa-edit text-primary"></i></a>';
                

                return $ac;

             })
            ->rawColumns([
                'count',
                'image',
                'action',
                'description',
            ])
            
            ->make(true);
    }

    public function new_listing(){
    	return view('admin.listing.add_listing');
    }

    public function add_listing(Request $request){
    	$credentials = $request->only('title', 'location', 'detail', 'price', 'picture');
		$rules = [

			'title' => 'required',
			'location' => 'required',
			'price' => 'required',
			'detail' => 'required',
			'picture' => 'required',
		];
		$validator = Validator::make($credentials, $rules);
		if ($validator->fails()) {
			return back()->with('error', $validator->messages());
		}

		$picture = uniqid() . '.' . $request->picture->getClientOriginalExtension("imageQuestion");
		$request->picture->move(public_path("images/listing"), $picture);

		$imagepath = url("/public/images/listing/" . $picture);

		$add_listing = Listing::create([
			'title' => $request->title,
			'location' => $request->location,
			'detail' => $request->detail,
			'price' => $request->price,
			'picture' => $picture,
		]);
		if($add_listing){
            $transition = Transition::all();
			$listings = Listing::all();
			return view('admin.listing.listings', compact('listings'))->with('message', 'Listing created successfully')->with('transition', $transition);
		}
    }


    public function edit_listing($id){
    	$listing = Listing::where('id', $id)->first();
        $transition = Transition::all();
    	if($listing){
    		return view('admin.listing.edit_listing', compact('listing'), compact('transition'));
    	}
    }


    public function update_listing(Request $request){
    	$credentials = $request->only('id', 'title', 'location', 'detail', 'price', 'picture');
		$rules = [
			'id' => 'required',
			'title' => 'required',
			'location' => 'required',
			'price' => 'required',
			'detail' => 'required',
		];
		$validator = Validator::make($credentials, $rules);
		if ($validator->fails()) {
			return back()->with('error', $validator->messages());
		}

		if(!empty($request->picture)){

			$picture = uniqid() . '.' . $request->picture->getClientOriginalExtension("imageQuestion");
			$request->picture->move(public_path("images/listing"), $picture);

			$imagepath = url("/public/images/listing/" . $picture);

			$add_listing = Listing::where('id', $request->id)->update([
				'title' => $request->title,
				'location' => $request->location,
				'detail' => $request->detail,
				'price' => $request->price,
				'picture' => $picture,
			]);
		}
		else{
			$add_listing = Listing::where('id', $request->id)->update([
				'title' => $request->title,
				'location' => $request->location,
				'detail' => $request->detail,
				'price' => $request->price,
			]);
		}
        
		if($add_listing){
			$listings = Listing::all();
            $transition = Transition::all();
			return view('admin.listing.listings', compact('listings'))->with('message', 'Listing Updated successfully')->with('transition', $transition);
		}
    }

    public function delete_listing(Request $request){
    	$listing_delete = Listing::where('id', $request->id)->delete();
        if($listing_delete){
            return 1;
        }
        else{
            return 0;
        }
    }

    public function transition(Request $request){
        $transition = Transition::all();
        if(!empty($transition[0]->id)){
            $update_transition = Transition::where('id', $transition[0]->id)->update([
                'speed' => $request->speed,
                'time_out' => $request->time_out,
            ]);
            if($update_transition){
                return 1;
            }
        }
        else{
           $add_transition = Transition::create([
                'speed' => $request->speed,
                'time_out' => $request->time_out,
           ]);
           if($add_transition){
                return 1;
           }
        }
        
    }


    public function pause_listing(Request $request){
        $listing_block = Listing::where('id', $request->id)->update([
            'is_block' => 1,
        ]);
        if($listing_block){
            return $request->id;
        }
        else{
            return 0;
        }
    }

    public function play_listing(Request $request){
        $listing_block = Listing::where('id', $request->id)->update([
            'is_block' => 0,
        ]);
        if($listing_block){
            return $request->id;
        }
        else{
            return 0;
        }
    }
}
