<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriberMessage extends Model
{
    //
    protected $fillable = [
    	'email', 'message',
    ];
}
